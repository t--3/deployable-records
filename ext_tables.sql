#
# Table structure for table 'tx_deployable_records'
#
CREATE TABLE tx_deployable_records (
  identifier varchar(85) DEFAULT '' NOT NULL,
  table varchar(255) DEFAULT '' NOT NULL,
	uid int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,

	PRIMARY KEY (identifier)
);

#
# Table structure for table 'tx_deployable_records_local_identifier'
#
CREATE TABLE tx_deployable_records_local_identifier (
	local_identifier varchar(85) DEFAULT '' NOT NULL,
	parent varchar(85) DEFAULT '' NOT NULL,

	PRIMARY KEY (local_identifier, parent),
	KEY identifier (parent)
);
