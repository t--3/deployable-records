define([
    'jquery',
    'handlebars',
    'TYPO3/CMS/Backend/Modal',
    'TYPO3/CMS/Backend/Notification',
    'TYPO3/CMS/DeployableRecords/Component/Modal/New',
    'TYPO3/CMS/DeployableRecords/Component/Modal/Edit',
    'TYPO3/CMS/DeployableRecords/Component/Events'
], function($, Handlebars, Modal, Notification) {
    'use strict';

    // TODO: Remove
    Handlebars.registerHelper('debug', function(data) {
        console.log('Debug Template', data);
    });

    // Extend jQuery's serializeArray function, to include unchecked checkboxes.
    var originalSerializeArray = $.fn.serializeArray;
    $.fn.extend({
        serializeArray: function () {
            var brokenSerialization = originalSerializeArray.apply(this);
            var checkboxValues = $(this).find('input[type=checkbox]').map(function () {
                return { 'name': this.name, 'value': this.checked };
            }).get();
            var checkboxKeys = $.map(checkboxValues, function (element) { return element.name; });
            var withoutCheckboxes = $.grep(brokenSerialization, function (element) {
                return $.inArray(element.name, checkboxKeys) == -1;
            });

            return $.merge(withoutCheckboxes, checkboxValues);
        }
    });
});
