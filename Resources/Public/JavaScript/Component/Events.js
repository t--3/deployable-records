define([
    'jquery',
    'TYPO3/CMS/Backend/Notification'
], function($, Notification) {
    'use strict';

    // Register events
    $('a.linksync').click(function() {
        $.post(TYPO3.settings.ajaxUrls['deployable_records_sync'], {
            'direction': $(this).data('direction')
        }, function(data) {
            if (data.status === 'ok') {
                Notification.success(data.title || 'Success', data.message);
                window.location.reload();
                return;
            }
            Notification.error(data.title || 'An error occurred!', data.message);
        });
    });

    $('a.linkupdate').click(function() {
        $.post(TYPO3.settings.ajaxUrls['deployable_records_update'], {
            'identifier': $(this).data('identifier'),
            'table': $(this).data('table'),
            'uid': $(this).data('uid'),
            'direction': $(this).data('direction')
        }, function(data) {
            if (data.status === 'ok') {
                Notification.success(data.title || 'Success', data.message);
                window.location.reload();
                return;
            }
            Notification.error(data.title || 'An error occurred!', data.message);
        });
    });

    $('a.linkremove').click(function() {
        $.post(TYPO3.settings.ajaxUrls['deployable_records_remove'], {
            'identifier': $(this).data('identifier')
        }, function(data) {
            if (data.status === 'ok') {
                Notification.success(data.message);
                Modal.dismiss();
                window.location.reload();
                return;
            }

            Notification.error('An error occurred!', data.message);
        });
    });

    $('a.linkupload').click(function() {
        $.post(TYPO3.settings.ajaxUrls['deployable_records_upload'], {
            'identifier': $(this).data('identifier'),
            'table': $(this).data('table')
        }, function(data) {
            if (data.status === 'ok') {
                Notification.success(data.title || 'Success', data.message);
                window.location.reload();
                return;
            }
            Notification.error(data.title || 'An error occurred!', data.message);
        });
    });
});
