define([
    'jquery',
    'handlebars',
    'TYPO3/CMS/Backend/Modal',
    'TYPO3/CMS/Backend/Notification'
], function($, Handlebars, Modal, Notification) {
    'use strict';

    var openNewModalsCount = 0;
    var openNewModals = {};
    var currentLinkData = null;

    var configurationNew = {
        type: Modal.types.default,
        title: 'New deployable record',
        additionalCssClasses: ['deployable-records', 'modal-new'],
        content: '',
        size: Modal.sizes.default,
        callback: function(currentModal) {
            var templateSource;
            var getStatusAndRenderView = function(callback) {
                $.post(TYPO3.settings.ajaxUrls['deployable_records_status'], {
                    'table': $(currentModal).data('settings').table,
                    'uid': $(currentModal).data('settings').uid
                }, function(data) {
                    $(currentModal).find('> .modal-dialog >.modal-content > .modal-body').css('visibility', 'hidden');
                    var missingRecords = {};
                    var missingRecordsCount = 0;
                    for (var key in data.status.missingRelations) {
                        var items = data.status.missingRelations[key];
                        $(items).each(function(index, item) {
                            item.key = missingRecords[item.table + ':' + item.uid]
                                ? missingRecords[item.table + ':' + item.uid].key + ', ' + key
                                : key;
                            missingRecords[item.table + ':' + item.uid] = item;
                            missingRecordsCount++;
                        });
                    }


                    var template = Handlebars.compile(templateSource);
                    var context = {
                        'status': data.status,
                        'missingRelatedRecords': missingRecords,
                        'missingRelatedRecordsCount': missingRecordsCount
                    };
                    var html = template(context);
                    $(currentModal).find('> .modal-dialog >.modal-content > .modal-body')
                        .html(html)
                        .off('click', '.identifier-suggestion')
                        .on('click', '.identifier-suggestion', function(event) {
                            event.preventDefault();
                            $(currentModal).find('[name="identifier"]:visible').last().val($(this).text());
                            $(currentModal).find('[name="identifier"]:visible').last().focus();
                        })
                        .off('click', '.deployable-records-dump')
                        .on('click', '.deployable-records-dump', openModal)
                        .css('visibility', 'visible');

                    if (callback) {
                        callback();
                    }
                });
            };

            // Fetch template
            $.get(TYPO3.settings.ajaxUrls.login.replace(
                /(.*typo3)\/.*/g,
                '$1conf/ext/deployable_records/Resources/Public/JavaScript/Component/Modal/New.html'
            ), function(data) {
                templateSource = data;
                $(currentModal).data('modalIndex', currentLinkData.index);
                $(currentModal).data('settings', currentLinkData.data);
                openNewModals[currentLinkData.index] = currentModal;

                currentModal.find('> .modal-dialog >.modal-content > .modal-body').css('visibility', 'hidden');
                getStatusAndRenderView(function(){

                    $(currentModal).on('refresh-contents', function() {
                        getStatusAndRenderView(function(){
                            setTimeout(function(){
                                $(currentModal).find('[name="identifier"]:visible').last().focus();
                            }, 500);
                        });
                    });

                    setTimeout(function(){
                        $(currentModal).find('[name="identifier"]:visible').last().focus();
                    }, 500);
                });

                currentModal.submit(function(event) {
                    event.preventDefault();

                    var baum = $(currentModal).find('form').serializeArray();
                    var dataToSubmit = {};
                    var options = {};
                    for (var i = 0; i < baum.length; i++) {
                        var item = baum[i];
                        if (!item.name.search(/options\./)) {
                            var optionName = item.name.replace(/options\.(.*)/, '$1');
                            options[optionName] = item.value;
                        } else {
                            dataToSubmit[item.name] = item.value;
                        }
                    }
                    dataToSubmit.options = options;

                    // Create new record
                    $.post(TYPO3.settings.ajaxUrls['deployable_records_add'], dataToSubmit, function(data) {
                        if (data.status === 'ok') {
                            Notification.success(data.title || 'Success', data.message);
                            var doReload = openNewModalsCount === 1;
                            Modal.dismiss();
                            if (doReload) {
                                window.location.reload();
                            }
                            return;
                        }

                        Notification.error(data.title || 'An error occurred!', data.message);
                    });
                });

                currentModal.on('hide.bs.modal', function() {
                    openNewModalsCount--;
                    if (openNewModalsCount > 0) {
                        $(openNewModals[openNewModalsCount]).trigger('refresh-contents');
                    }
                });
            });
        }
    };

    // Register event
    var openModal = function() {
        openNewModalsCount++;
        if ($(this).hasClass('active')) return;
        currentLinkData = {data: $(this).data(), index: openNewModalsCount};
        Modal.advanced(configurationNew, $(this).data());
    };
    $(document).on('click', '.deployable-records-dump', openModal);

});
