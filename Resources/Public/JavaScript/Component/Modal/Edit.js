define([
    'jquery',
    'handlebars',
    'TYPO3/CMS/Backend/Modal',
    'TYPO3/CMS/Backend/Notification'
], function($, Handlebars, Modal, Notification) {
    'use strict';

    var currentLinkData = null;

    var configurationEdit = {
        type: Modal.types.ajax,
        title: 'Edit deployable record',
        content: TYPO3.settings.ajaxUrls.login.replace(
            /(.*typo3)\/.*/g,
            '$1conf/ext/deployable_records/Resources/Public/JavaScript/Component/Modal/Edit.html'
        ),
        size: Modal.sizes.default,
        callback: function(currentModal) {
            // TODO Ask before removing... And check children!
            $.post(TYPO3.settings.ajaxUrls['deployable_records_status'], {
                'identifier': currentModal.find('#identifier').val(),
                'table': currentLinkData.table,
                'uid': currentLinkData.uid
            }, function(data) {
                console.log(data);
            });

            currentModal.submit(function(event) {
                event.preventDefault();

                // Remove record
                $.post(TYPO3.settings.ajaxUrls['deployable_records_remove'], {
                    'identifier': currentLinkData.identifier
                }, function(data) {
                    if (data.status === 'ok') {
                        currentLinkData = null;
                        Notification.success(data.message);
                        Modal.dismiss();
                        window.location.reload();
                        return;
                    }

                    Notification.error('An error occurred!', data.message);
                });
            });
        }
    };

    // Register event
    $('.deployable-records-dump.active').click(function() {
        currentLinkData = $(this).data();
        Modal.advanced(configurationEdit, $(this).data());
    });

});
