<?php declare(strict_types=1);
namespace T3\DeployableRecords\Command;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use T3\DeployableRecords\Dump\DumpController;
use T3\DeployableRecords\Dump\DumpStatus;

/**
 * StatusCommand
 */
class StatusCommand extends Command
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setDescription('Deployable Record Status');
        $this->setHelp('Get status of deployable records.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \T3\DeployableRecords\Dump\DumpException
     * @throws \T3\DeployableRecords\Dump\IO\IOException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $dumpController = new DumpController();

        // This throws IOException when Deployable Records not correctly configured
        (new \T3\DeployableRecords\Configuration())->isConfiguredCorrectly();

        $status = $dumpController->getStatusForAll();

        if ($status->hasErrors()) {
            $io->warning(\count($status->getRecordsWithErrors()) . ' records have got errors!');
            return;
        } elseif ($status->isAllInSync()) {
            $io->success('All in sync!');
            return;
        } else {
            $io->warning(\count($status->getRecordsOutOfSync()) . ' records are out of sync!');
        }

        $table = ['#', 'identifier', 'table', 'uid', 'status'];
        $rows = [];
        /** @var DumpStatus $dumpStatus */
        foreach ($status->getRecordsOutOfSync() as $i => $dumpStatus) {
            $rows[] = [
                $i,
                $dumpStatus->getIdentifier(),
                $dumpStatus->getTable(),
                $dumpStatus->getRow() ? $dumpStatus->getRow()['uid'] : ' - ',
                $dumpStatus->getStatus()
            ];
        }

        $io->table($table, $rows);
    }
}
