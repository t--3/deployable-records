<?php declare(strict_types=1);
namespace T3\DeployableRecords\Command;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use T3\DeployableRecords\Dump\DumpController;

/**
 * SyncCommand
 */
class SyncCommand extends Command
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setDescription('Deployable Record Sync');
        $this->setHelp('This command requires a given --direction option. Allowed values are "db" or "dump".');
        $this->addOption('direction', null, InputOption::VALUE_REQUIRED, 'Allowed values are: "db" or "dump"');
        $this->addOption('db', null, InputOption::VALUE_NONE, 'Alias for --direction="db"');
        $this->addOption('dump', null, InputOption::VALUE_NONE, 'Alias for --direction="dump"');
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('direction') && !$input->getOption('db') && !$input->getOption('dump')) {
            $io = new SymfonyStyle($input, $output);
            $direction = $io->ask('In which direction you want to sync/write (db or dump)?', null, function ($value) {
                if (empty($value)) {
                    throw new \InvalidArgumentException('No direction given!');
                }
                if ($value !== 'db' && $value !== 'dump') {
                    throw new \InvalidArgumentException('Invalid direction given! "db" or "dump" is allowed.');
                }
                return $value;
            });
            $input->setOption('direction', $direction);
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \T3\DeployableRecords\Dump\DumpException
     * @throws \T3\DeployableRecords\Dump\IO\IOException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $direction = $input->getOption('direction');
        if (!$direction && $input->getOption('db')) {
            $direction = 'db';
        }
        if (!$direction && $input->getOption('dump')) {
            $direction = 'dump';
        }
        if ($direction !== 'db' && $direction !== 'dump') {
            throw new \InvalidArgumentException('Option "direction" required. Allowed values are "db" and "dump".');
        }
        $io = new SymfonyStyle($input, $output);
        $dumpController = new DumpController();
        $status = $dumpController->syncAll($direction);
        $status ? $io->success('All records synced to ' . $direction . '!') : $io->error('Sync failed!');
    }
}
