<?php declare(strict_types=1);
namespace T3\DeployableRecords\Ajax;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use Psr\Http\Message\ServerRequestInterface;
use T3\DeployableRecords\Domain\Repository\AnyRecordRepository;
use T3\DeployableRecords\Domain\Repository\DeployableRecordRepository;
use T3\DeployableRecords\Dump\DumpController;
use T3\DeployableRecords\Dump\DumpException;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class Records
 */
class RecordsAjaxController
{
    /**
     * @var DumpController
     */
    protected $dumpController;

    /**
     * @var DeployableRecordRepository
     */
    protected $deployableRecordRepository;

    /**
     * @var AnyRecordRepository
     */
    protected $anyRecordRepository;

    /**
     * Initializes this controller on each action
     *
     * @param ServerRequestInterface $request
     * @param Response $response
     * @return bool If false, the action should get aborted
     */
    protected function init(ServerRequestInterface $request, Response $response) : bool
    {
        try {
            $this->dumpController = GeneralUtility::makeInstance(DumpController::class);
            $this->deployableRecordRepository = GeneralUtility::makeInstance(DeployableRecordRepository::class);
            $this->anyRecordRepository= GeneralUtility::makeInstance(AnyRecordRepository::class);
        } catch (\Exception $e) {
            $response->getBody()->write($this->exceptionToJson($e));
            return false;
        }
        return true;
    }

    /**
     * Get status of deployable records (database records)
     *
     * @param ServerRequestInterface $request
     * @param Response $response
     * @return Response
     * @throws DumpException
     */
    public function statusAction(ServerRequestInterface $request, Response $response) : Response
    {
        if (!$this->init($request, $response)) {
            return $response;
        }
        $postParameters = $request->getParsedBody();

        // Validate input
        if (!$this->validatePostParameters($postParameters, false)) {
            return $response;
        }

        try {
            $status = $this->dumpController->getDumpStatus(
                $postParameters['identifier'] ?: null,
                $postParameters['table'] ?: null,
                (int) $postParameters['uid'] ?: null
            );
        } catch (DumpException $exception) {
            $response->getBody()->write($this->exceptionToJson($exception));
            return $response;
        }

        $response->getBody()->write(json_encode(['status' => $status->toArray()]));
        return $response;
    }


    /**
     * Create new deployable record, based on existing database record
     *
     * @param ServerRequestInterface $request
     * @param Response $response
     * @return Response
     */
    public function addAction(ServerRequestInterface $request, Response $response)
    {
        if (!$this->init($request, $response)) {
            return $response;
        }
        $postParameters = $request->getParsedBody();

        // Validate input
        if (!$postParameters || true !== $this->validatePostParameters($postParameters)) {
            $response->getBody()->write(json_encode([
                'status' => 'error',
                'message' => $this->validatePostParameters($postParameters, false)
            ]));
            return $response;
        }

        $identifier = $postParameters['identifier'];

        $table = $postParameters['table'];
        $uid = (int) $postParameters['uid'];
        $options = $postParameters['options'] ?? [];

        try {
            $this->dumpController->addDeployableRecord($identifier, $table, $uid, false, $options);
        } catch (DumpException $exception) {
            $response->getBody()->write($this->exceptionToJson($exception));
            return $response;
        }

        $response->getBody()->write(json_encode([
            'status' => 'ok',
            'identifier' => $identifier,
            'message' => 'New deployable record with identifier "' . $identifier .
                         '" created and stored in dump file.'
        ]));
        return $response;
    }

    /**
     * Removes deployable record from db and dump
     *
     * @param ServerRequestInterface $request
     * @param Response $response
     * @return Response
     * @TODO use DumpController
     */
    public function removeAction(ServerRequestInterface $request, Response $response) : Response
    {
        if (!$this->init($request, $response)) {
            return $response;
        }
        $postParameters = $request->getParsedBody();

        if (!isset($postParameters['identifier'])) {
            $response->getBody()->write(json_encode([
                'status' => 'error',
                'message' => 'Argument "identifier" not given!'
            ]));
            return $response;
        }

        $identifier = $postParameters['identifier'];

        $deployableRecord = $this->deployableRecordRepository->findByIdentifier($identifier);
        if (!$deployableRecord) {
            $response->getBody()->write(json_encode([
                'status' => 'error',
                'message' => 'No deployable record with identifier "' . $identifier . '" found.'
            ]));
            return $response;
        }

        $status = $this->deployableRecordRepository->remove($deployableRecord);
        // TODO What about child entities?

        if (!$status) {
            $response->getBody()->write(json_encode([
                'status' => 'error',
                'message' => 'Error while removing deployable record with identifier "' . $identifier . '".'
            ]));
            return $response;
        }
        $response->getBody()->write(json_encode([
            'status' => 'ok',
            'message' => 'Deployable record with identifier "' . $identifier . '" deleted.'
        ]));
        // TODO Add "dump now" button if autoDump is disabled
        return $response;
    }

    /**
     * @param ServerRequestInterface $request
     * @param Response $response
     * @return Response
     * @throws DumpException
     * @throws \T3\DeployableRecords\Dump\IO\IOException
     */
    public function updateAction(ServerRequestInterface $request, Response $response) : Response
    {
        if (!$this->init($request, $response)) {
            return $response;
        }
        $postParameters = $request->getParsedBody();
        if (true !== $this->validatePostParameters($postParameters, false) ||
            true !== $this->validateDirectionFromPostParameters($postParameters)
        ) {
            $message = $this->validatePostParameters($postParameters, false);
            if ($message === true) {
                $message = $this->validateDirectionFromPostParameters($postParameters);
            }
            $response->getBody()->write(json_encode([
                'status' => 'error',
                'message' => $message
            ]));
            return $response;
        }

        $this->dumpController->updateDeployableRecord(
            $postParameters['direction'],
            $postParameters['table'],
            $postParameters['identifier'],
            $postParameters['options'] ?? []
        );

        $directionText = $postParameters['direction'] === 'db' ? 'database row' : 'dump row';
        $response->getBody()->write(json_encode([
            'status' => 'ok',
            'message' => 'Updated ' . $directionText . ' with identifier "' . $postParameters['identifier'] . '"'
        ]));
        return $response;
    }

    /**
     * @param ServerRequestInterface $request
     * @param Response $response
     * @return Response
     * @throws DumpException
     * @throws \T3\DeployableRecords\Dump\IO\IOException
     */
    public function syncAction(ServerRequestInterface $request, Response $response) : Response
    {
        if (!$this->init($request, $response)) {
            return $response;
        }
        $postParameters = $request->getParsedBody();
        if (!$this->validateDirectionFromPostParameters($postParameters)) {
            return $response;
        }
        $status = $this->dumpController->syncAll($postParameters['direction']);

        if ($status) {
            $response->getBody()->write(json_encode([
                'status' => 'ok',
                'message' => 'All records successfully synced to ' . $postParameters['direction'] . '.'
            ]));
        } else {
            $response->getBody()->write(json_encode([
                'status' => 'error',
                'message' => 'During sync an error occurred!'
            ]));
        }
        return $response;
    }

    /**
     * Similar to addAction, but also creates the database record which should be added to Deployable Records
     *
     * @param ServerRequestInterface $request
     * @param Response $response
     * @return Response
     * @throws DumpException
     * @throws \T3\DeployableRecords\Dump\IO\IOException
     */
    public function uploadAction(ServerRequestInterface $request, Response $response) : Response
    {
        if (!$this->init($request, $response)) {
            return $response;
        }
        $postParameters = $request->getParsedBody();
        $identifier = $postParameters['identifier'];
        if (!isset($identifier, $postParameters['table'])) {
            $response->getBody()->write(json_encode([
                'status' => 'error',
                'message' => 'Argument "identifier" or "table" not given!'
            ]));
            return $response;
        }

        $status = $this->dumpController->createDeployableRecord($identifier); // TODO options
        if (!$status) {
            $response->getBody()->write(json_encode([
                'status' => 'error',
                'message' => 'Error while inserting new record based on deployable record in dump with identifier "' .
                             $identifier . '".'
            ]));
            return $response;
        }
        $response->getBody()->write(json_encode([
            'status' => 'ok',
            'message' => 'Deployable record with identifier "' . $identifier . '" imported.'
        ]));
        // TODO Add "dump now" button if autoDump is disabled
        return $response;
    }


    /**
     * Validates given post parameters for new deployable record.
     *
     * @param array $postParameters
     * @param bool $strict Strict means "identifier", "table" and "uid" are required.
     *                     Set this to false to require "identifier" or "table" and "uid"
     * @return bool|string True if valid. Otherwise string with error message.
     */
    private function validatePostParameters(array $postParameters, bool $strict = true)
    {
        $errorIdentifier = !isset($postParameters['identifier']);
        $errorTable = !isset($postParameters['table']);
        $errorUid = !isset($postParameters['uid']);

        if (!$strict) {
            if ($errorIdentifier || $errorTable & $errorUid) {
                return true;
            }
        }
        if ($errorIdentifier) {
            return 'Argument "identifier" not given!';
        }
        if ($errorTable || $errorUid) {
            return 'Argument "table" or "uid" not given!';
        }
        return true;
    }

    /**
     * @param array $postParameters
     * @return bool|string True if valid. Otherwise string with error message.
     */
    private function validateDirectionFromPostParameters(array $postParameters)
    {
        if (!isset($postParameters['direction'])) {
            return 'Argument "direction" not given!';
        }
        if ($postParameters['direction'] !== DumpController::FROM_DUMP_TO_DB &&
            $postParameters['direction'] !== DumpController::FROM_DB_TO_DUMP
        ) {
            return 'Argument "direction" allowed values are "db" or "dump"!';
        }
        return true;
    }

    /**
     * Helper function to convert exceptions to json strings
     *
     * @param \Exception $exception
     * @return string json string
     */
    private function exceptionToJson(\Exception $exception) : string
    {
        $error = [
            'status' => 'error',
            'code' => $exception->getCode(),
            'message' => $exception->getMessage()
        ];
        $prettyPrint = 0;
        if (isset($GLOBALS['TYPO3_CONF_VARS']['BE']['DEBUG']) && $GLOBALS['TYPO3_CONF_VARS']['BE']['DEBUG']) {
            $error['file'] = $exception->getFile();
            $error['line'] = $exception->getLine();
            $prettyPrint = JSON_PRETTY_PRINT;
        }
        return json_encode($error, $prettyPrint);
    }
}
