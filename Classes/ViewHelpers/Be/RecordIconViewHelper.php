<?php declare(strict_types=1);
namespace T3\DeployableRecords\ViewHelpers\Be;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2012-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Returns the icon of a specific record
 */
class RecordIconViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @var bool
     */
    protected $escapeChildren = false;

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * @var IconFactory
     */
    private static $iconFactory;

    /**
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        static::$iconFactory = GeneralUtility::makeInstance(IconFactory::class);
    }

    /**
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('table', 'string', '', true);
        $this->registerArgument('row', 'array', '', false, []);
        $this->registerArgument('size', 'string', 'Available: default, small, large', false, Icon::SIZE_DEFAULT);
    }

    /**
     * @return string HTML output for svg icon
     */
    public function render() : string
    {
        if (empty($this->arguments['row'])) {
            $this->arguments['row'] = $this->renderChildren() ?? [];
        }
        if ($this->arguments['table'] === 'pages' && $this->arguments['row']['uid'] === 0) {
            return static::$iconFactory->getIcon('information-typo3-version', Icon::SIZE_SMALL)->render();
        }
        return static::$iconFactory->getIconForRecord(
            $this->arguments['table'],
            $this->arguments['row'],
            $this->arguments['size']
        )->render();
    }
}
