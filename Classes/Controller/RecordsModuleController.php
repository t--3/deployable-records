<?php declare(strict_types=1);
namespace T3\DeployableRecords\Controller;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Configuration;
use T3\DeployableRecords\Domain\Repository\DeployableRecordRepository;
use T3\DeployableRecords\Dump\DumpController;
use T3\DeployableRecords\Dump\DumpException;
use T3\DeployableRecords\Dump\DumpStatus;
use T3\DeployableRecords\Dump\IO\IOException;
use TYPO3\CMS\Backend\Tree\Repository\PageTreeRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;

/**
 * Extbase controller
 * used by backend module
 */
class RecordsModuleController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var Configuration
     */
    protected $pageTsUtility;

    /**
     * @var DeployableRecordRepository
     */
    protected $deployableRecordsRepository;

    /**
     * @var PageTreeRepository
     */
    protected $pageTreeRepository;

    /**
     * @var DumpController
     */
    protected $dumpController;

    /**
     * Dependency Injection on my own
     *
     * @return void
     * @throws StopActionException
     */
    public function initializeAction()
    {
        $this->pageTsUtility = $this->objectManager->get(Configuration::class);
        try {
            $this->pageTsUtility->isConfiguredCorrectly();
        } catch (IOException $e) {
            if ($this->actionMethodName !== 'configurationAction') {
                $this->forward('configuration');
                return;
            }
        }
        $this->deployableRecordsRepository = $this->objectManager->get(DeployableRecordRepository::class);
        $this->pageTreeRepository = $this->objectManager->get(PageTreeRepository::class, 0, ['TSconfig']);
        $this->dumpController = $this->objectManager->get(DumpController::class);

        $this->settings['_action'] = substr($this->actionMethodName, 0, \strlen('Action') * -1);
        $this->settings['_amount_records'] = $this->deployableRecordsRepository->countAll();
    }

    /**
     * @return void
     * @throws DumpException
     * @throws StopActionException
     */
    public function indexAction()
    {
        $outputPath = GeneralUtility::getFileAbsFileName($this->pageTsUtility->get('tx_deployable_records.output'));
        $this->view->assign('outputPath', $outputPath);
        $this->view->assign('configuredOutputPath', $this->pageTsUtility->get('tx_deployable_records.output'));
        if ($outputPath) {
            // TODO fetch entities from dump
            $this->view->assign('outputPathExists', file_exists($outputPath));
            $this->view->assign('outputPathWritable', is_writable(\dirname($outputPath)));
            $this->view->assign('autoDumpEnabled', $this->pageTsUtility->get('tx_deployable_records.autoDump'));
        }

        try {
            $this->view->assign('status', $this->dumpController->getStatusForAll());
        } catch (IOException $e) {
            $this->forward('configuration');
        }
    }

    /**
     * @throws DumpException
     * @throws IOException
     */
    public function recordsAction()
    {
        $statusForAll = $this->dumpController->getStatusForAll();

        $resolver = new \Usox\Circulon\Circulon();
        $resolver->addDependency(0, []);

        /** @var DumpStatus[] $pages */
        $pages = [];
        /** @var DumpStatus[][] $records */
        $records = [];
        foreach ($statusForAll as $status) {
            if ($status->getTable() === 'pages') {
                $pages[$status->getRow()['uid']] = $status;
                $resolver->addDependency($status->getRow()['uid'], [$status->getRow()['pid']]);
            } else {
                if (!isset($records[$status->getRow()['pid']])) {
                    $records[$status->getRow()['pid']] = [];
                }
                $records[$status->getRow()['pid']][$status->getTable() . ':' . $status->getRow()['uid']] = $status;
            }
        }

        /** @var DumpStatus[] $resolvedPagesFlat */
        $resolvedPagesFlat = [];
        foreach ($resolver->resolve() as $pageUid) {
            if (empty($pageUid)) {
                continue;
            }
            $resolvedPagesFlat[$pageUid] = $pages[$pageUid];
        }

        $rootLineDumpStatuses = $this->buildRootLineDumpStatuses($resolvedPagesFlat, $records);

        $this->view->assign('rootLineDumpStatuses', $rootLineDumpStatuses);
        $this->view->assign('deployableRecordsStatus', $this->dumpController->getStatusForAll());
        $this->view->assign('sitename', $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename']);
    }

    /**
     * @param DumpStatus[] $resolvedPagesFlat
     * @param DumpStatus[][] $records
     * @param int $pageUid
     * @return array with keys "page", "children" and "records"
     */
    protected function buildRootLineDumpStatuses(array $resolvedPagesFlat, array $records, int $pageUid = 0) : array
    {
        $nestedPages = [
            'page' => $resolvedPagesFlat[$pageUid] ?? null,
            'children' => [],
            'records' => []
        ];

        foreach ($records[$pageUid] ?? [] as $rootRecord) {
            if ($rootRecord->getRow()['pid'] === $pageUid) {
                if (!isset($nestedPages['records'][$rootRecord->getTable()])) {
                    $nestedPages['records'][$rootRecord->getTable()] = [];
                }
                $nestedPages['records'][$rootRecord->getTable()][$rootRecord->getRow()['uid']] = $rootRecord;
            }
        }
        foreach ($resolvedPagesFlat as $rootPage) {
            if ($rootPage->getRow()['pid'] === $pageUid) {
                $nestedPages['children'][] = $this->buildRootLineDumpStatuses(
                    $resolvedPagesFlat,
                    $records,
                    $rootPage->getRow()['uid']
                );
            }
        }
        return $nestedPages;
    }

    /**
     *
     */
    public function configurationAction()
    {
        $configTree = [
            0 => $this->pageTsUtility->get('tx_deployable_records', [], 0)
        ];
        $configTree[0]['outputReal'] = GeneralUtility::getFileAbsFileName(
            $this->pageTsUtility->get('tx_deployable_records.output')
        );

        $tree = $this->pageTreeRepository->getTree(0);

        /**
         * @param array $tree
         * @param array $pages
         * @return array with keys "row" and "diff". key is the page uid.
         */
        $getPagesWithExtraConfig = function (
            array $tree,
            array $pages = []
        ) use (
            &$getPagesWithExtraConfig,
            $configTree
        ) {
            foreach ($tree as $row) {
                if ($row['TSconfig']) {
                    $pageConfig = $this->pageTsUtility->get('tx_deployable_records', [], $row['uid']);
                    if ($diff = array_diff_assoc($pageConfig, $configTree[0])) {
                        $pages[$row['uid']] = [
                            'row' => $row,
                            'diff' => $diff
                        ];
                    }
                }
                if (isset($row['_children'])) {
                    $getPagesWithExtraConfig($row['_children'], $pages);
                }
            }
            return $pages;
        };
        $configTree += $getPagesWithExtraConfig($tree['_children']);
        $this->view->assign('configTree', $configTree);
    }
}
