<?php declare(strict_types=1);
namespace T3\DeployableRecords\Domain\Model;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Domain\Repository\AnyRecordRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class DeployableRecord
 */
class DeployableRecord
{
    /**
     * @var string
     */
    protected $identifier;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var int
     */
    protected $uid;

    /**
     * @var \DateTime
     */
    protected $crdate;

    /**
     * DeployableRecord constructor
     *
     * @param string $identifier
     * @param string $table
     * @param int $uid
     * @param \DateTime|null $crdate
     */
    public function __construct(
        string $identifier,
        string $table,
        int $uid,
        \DateTime $crdate = null
    ) {
        $this->identifier = $identifier;
        $this->table = $table;
        $this->uid = $uid;
        try {
            $this->crdate = $crdate ?? new \DateTime();
        } catch (\Exception $e) {
        }
    }

    /**
     * @return string
     */
    public function getIdentifier() : string
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getTable() : string
    {
        return $this->table;
    }

    /**
     * @return int
     */
    public function getUid() : int
    {
        return $this->uid;
    }

    /**
     * @return \DateTime
     */
    public function getCrdate() : \DateTime
    {
        return $this->crdate;
    }

    /**
     * Returns database row from current environment
     *
     * @return array
     */
    public function getDatabaseRow() : array
    {
        /** @var AnyRecordRepository $anyRepository */
        $anyRepository = GeneralUtility::makeInstance(AnyRecordRepository::class);
        return $anyRepository->findByDeployableRecord($this);
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return [
            'identifier' => $this->getIdentifier(),
            'table' => $this->getTable(),
            'uid' => $this->getUid(),
            'crdate' => $this->getCrdate()->getTimestamp()
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getIdentifier();
    }
}
