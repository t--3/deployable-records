<?php declare(strict_types=1);
namespace T3\DeployableRecords\Domain\Repository;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Domain\Model\DeployableRecord;
use T3\DeployableRecords\Dump\DumpUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * This is not an Extbase repository!
 * It fetches any record by given table name and uid.
 */
class AnyRecordRepository implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * @var ConnectionPool
     */
    protected $connectionPool;

    /**
     * @var QueryBuilder[] key is table name
     */
    protected $queryBuilderStorage = [];

    /**
     * @var array
     */
    private $defaultsCache = [];

    /**
     * DeployableRecordRepository constructor
     */
    public function __construct()
    {
        $this->connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
    }

    /**
     * Returns an instance of QueryBuilder for given table
     * and caches it statically
     *
     * @param string $table
     * @return QueryBuilder
     */
    protected function getQueryBuilderForTable(string $table) : QueryBuilder
    {
        if (!array_key_exists($table, $this->queryBuilderStorage)) {
            $this->queryBuilderStorage[$table] = $this->connectionPool->getQueryBuilderForTable($table);
            $this->queryBuilderStorage[$table]->getRestrictions()->removeAll();
        }
        return clone $this->queryBuilderStorage[$table];
    }

    /**
     * @param string $table
     * @return array
     */
    public function findAllByTable(string $table) : array
    {
        $where = '';
        if (isset($GLOBALS['TCA'][$table]['ctrl']['delete'])) {
            $where = $GLOBALS['TCA'][$table]['ctrl']['delete'] . ' = 0';
        }

        $queryBuilder = $this->getQueryBuilderForTable($table);
        $queryBuilder
            ->select('*')
            ->from($table);

        if (!empty($where)) {
            $queryBuilder->where($where);
        }

        return $queryBuilder->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param string $table
     * @param int $uid
     * @return array
     */
    public function findByTableAndUid(string $table, int $uid) : array
    {
        $queryBuilder = $this->getQueryBuilderForTable($table);
        return $queryBuilder
            ->select('*')
            ->from($table)
            ->where($queryBuilder->expr()->eq('uid', $uid))
            ->execute()
            ->fetch(\PDO::FETCH_ASSOC) ?: [];
    }

    /**
     * @param DeployableRecord $deployableRecord
     * @return array
     */
    public function findByDeployableRecord(DeployableRecord $deployableRecord) : array
    {
        $queryBuilder = $this->getQueryBuilderForTable($deployableRecord->getTable());
        return $queryBuilder
            ->select('*')
            ->from($deployableRecord->getTable())
            ->where($queryBuilder->expr()->eq('uid', $deployableRecord->getUid()))
            ->execute()
            ->fetch(\PDO::FETCH_ASSOC) ?: [];
    }

    /**
     * @param string $table
     * @param int $uid
     * @param array $data
     * @return bool
     */
    public function update(string $table, int $uid, array $data) : bool
    {
        // Respect MM relations
        $data = $this->updateManyToManyRelations($table, $uid, $data);
        return (bool) $this->connectionPool->getConnectionForTable($table)->update(
            $table,
            $data,
            ['uid' => $uid]
        );
    }

    /**
     * @param string $identifier
     * @param array $dumpRow
     * @return int uid of inserted record or zero on error
     * @throws \T3\DeployableRecords\Dump\DumpException
     */
    public function createRecordFromDumpRow(string $identifier, array $dumpRow)
    {
        $table = $dumpRow['__table__'];
        $dbRow = DumpUtility::prepareDumpRow($identifier, $dumpRow, $table);

        $connection = $this->connectionPool->getConnectionForTable($table);
        $affectedRows = $connection->insert($table, $dbRow);

        if (!$affectedRows) {
            return 0;
        }
        return (int) $connection->lastInsertId($table);
    }

    /**
     * Returns the default values for all attributes of given table
     *
     * @param string $table
     * @return mixed
     */
    public function getDefaultValuesForTable(string $table)
    {
        if (array_key_exists($table, $this->defaultsCache)) {
            return $this->defaultsCache[$table];
        }

        $this->defaultsCache[$table] = [];
        $columns = $this->connectionPool->getConnectionForTable($table)->getSchemaManager()->listTableColumns($table);
        /** @var \Doctrine\DBAL\Schema\Column $column */
        foreach ($columns as $column) {
            $this->defaultsCache[$table][$column->getName()] = $column->getDefault();
        }
        return $this->defaultsCache[$table];
    }

    /**
     * Find many to many relations, based on TCA configuration
     *
     * @param int $uid
     * @param string $table
     * @param string $columnName
     * @return int[]
     */
    public function findManyToManyRelations(int $uid, string $table, string $columnName) : array
    {
        $foreignTable = $GLOBALS['TCA'][$table]['columns'][$columnName]['config']['foreign_table'];
        $mmTable = $GLOBALS['TCA'][$table]['columns'][$columnName]['config']['MM'];

        $queryBuilder = $this->getQueryBuilderForTable($table);
        $queryBuilder
            ->select('*')
            ->from($foreignTable)
            ->leftJoin(
                $foreignTable,
                $mmTable,
                'MM',
                $queryBuilder->expr()->eq('MM.uid_local', $queryBuilder->quoteIdentifier($foreignTable . '.uid'))
            )
            ->where(
                $queryBuilder->expr()->eq('MM.uid_foreign', $uid),
                $queryBuilder->expr()->eq('MM.tablenames', $queryBuilder->createNamedParameter($table)),
                $queryBuilder->expr()->eq('MM.fieldname', $queryBuilder->createNamedParameter($columnName))
            )
            ->orderBy(
                $GLOBALS['TCA'][$foreignTable]['ctrl']['sortby']
                    ? $foreignTable . '.' . $GLOBALS['TCA'][$foreignTable]['ctrl']['sortby']
                    : 'MM.sorting'
            );

        $result = $queryBuilder->execute()->fetchAll(\PDO::FETCH_ASSOC);

        $relations = [];
        foreach ($result as $relationRow) {
            $relations[] = (int) $relationRow['uid'];
        }
        return $relations;
    }

    /**
     * @param string $table
     * @param int $uid
     * @param array $data
     * @return array
     */
    private function updateManyToManyRelations(string $table, int $uid, array $data) : array
    {
        foreach ($data as $columnName => $value) {
            if (is_array($value) && array_key_exists('MM', $value) && array_key_exists('foreignTable', $value)) {
                $foreignTable = $value['foreignTable'];
                $mmTable = $value['MM'];
                $values = GeneralUtility::intExplode(',', $value['values'], true);

                $existing = $this->findManyToManyRelations($uid, $table, $columnName);
                $toDelete = array_diff($existing, $values);
                $toCreate = array_diff($values, $existing);
                $toUpdate = array_diff($existing, $toDelete, $toCreate);

                foreach (array_merge($toCreate, $toUpdate, $toDelete) as $index => $foreignRecordUid) {
                    $foreignRecord = $this->findByTableAndUid($foreignTable, $foreignRecordUid);
                    if (!$foreignRecord) {
                        // TODO: log missing relation
                        continue;
                    }

                    $columnValues = [
                        'uid_local' => $foreignRecordUid,
                        'uid_foreign' => $uid,
                        'tablenames' => $table,
                        'fieldname' => $columnName
                    ];

                    // Delete relation
                    if (in_array($foreignRecordUid, $toDelete, true)) {
                        $this->connectionPool->getConnectionForTable($mmTable)
                            ->delete(
                                $mmTable,
                                $columnValues
                            );
                    }

                    // Update sorting
                    if (in_array($foreignRecordUid, $toUpdate, true)) {
                        $this->connectionPool->getConnectionForTable($mmTable)
                            ->update(
                                $mmTable,
                                [
                                    'sorting' =>
                                        !$GLOBALS['TCA'][$foreignTable]['ctrl']['sortby'] ? ($index + 1) * 128 : 0,
                                    'sorting_foreign' =>
                                        $foreignRecord[$GLOBALS['TCA'][$foreignTable]['ctrl']['sortby']] ?: 0
                                ],
                                $columnValues
                            );
                    }

                    // no relation exists -> create
                    if (in_array((int)$foreignRecordUid['uid'], $toCreate, true)) {
                        $this->connectionPool->getConnectionForTable($mmTable)
                            ->insert(
                                $mmTable,
                                array_merge(
                                    [
                                        $columnValues,
                                        [
                                            'sorting' => !$GLOBALS['TCA'][$foreignTable]['ctrl']['sortby']
                                                ? ($index + 1) * 128
                                                : 0,
                                            'sorting_foreign' =>
                                                $foreignRecord[$GLOBALS['TCA'][$foreignTable]['ctrl']['sortby']] ?: 0
                                        ]
                                    ]
                                )
                            );
                    }
                }

                // Overwrite db value with amount of related entities
                $data[$columnName] = count($values);
            }
        }
        return $data;
    }
}
