<?php declare(strict_types=1);
namespace T3\DeployableRecords\Domain\Repository;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Domain\Model\DeployableRecord;
use T3\DeployableRecords\Domain\Repository\Helper\ReferringFieldHelper;
use T3\DeployableRecords\Dump\DumpException;
use T3\DeployableRecords\Dump\DumpReferenceException;
use TYPO3\CMS\Core\Configuration\FlexForm\FlexFormTools;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * This is not an Extbase repository!
 * It fetches the records from "tx_deployable_records" table and returns corresponding objects.
 *
 * Also it contains a method which converts uid/pids to identifiers and back.
 */
class DeployableRecordRepository implements \TYPO3\CMS\Core\SingletonInterface
{
    const TABLE = 'tx_deployable_records';

    /**
     * @var \TYPO3\CMS\Core\Database\Query\QueryBuilder
     */
    private $queryBuilder;

    /**
     * DeployableRecordRepository constructor
     */
    public function __construct()
    {
        $this->queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable(self::TABLE);
        $this->queryBuilder->getRestrictions()->removeAll();
    }

    /**
     * @return DeployableRecord[]
     * @throws \Exception
     */
    public function findAll() : array
    {
        // TODO: Cache by identifier
        $rows = $this->getQueryBuilder()
            ->select('*')
            ->from(self::TABLE)
            ->execute()
            ->fetchAll();

        $models = [];
        foreach ($rows as $row) {
            $models[] = $this->buildModel($row);
        }
        return $models;
    }

    /**
     * @return int
     */
    public function countAll() : int
    {
        return $this->getQueryBuilder()
            ->select('identifier')
            ->from(self::TABLE)
            ->execute()
            ->rowCount();
    }

    /**
     * @param string $identifier
     * @return null|DeployableRecord
     * @throws \Exception
     */
    public function findByIdentifier(string $identifier)
    {
        $queryBuilder = $this->getQueryBuilder();
        // TODO: Cache by identifier
        $row = $queryBuilder->select('*')->from(self::TABLE)->where(
            $queryBuilder->expr()->eq('identifier', $queryBuilder->createNamedParameter($identifier))
        )->execute()->fetch(\PDO::FETCH_ASSOC);
        return $row ? $this->buildModel($row) : null;
    }

    /**
     * @param string $table
     * @param int $uid
     * @return null|DeployableRecord
     * @throws \Exception
     */
    public function findOneByTableAndUid(string $table, int $uid)
    {
        $queryBuilder = $this->getQueryBuilder();
        // TODO: Cache by identifier
        $row = $queryBuilder->select('*')->from(self::TABLE)->where(
            $queryBuilder->expr()->eq('table', $queryBuilder->createNamedParameter($table)),
            $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT))
        )->execute()->fetch(\PDO::FETCH_ASSOC);
        return $row ? $this->buildModel($row) : null;
    }

    /**
     * @param array $row
     * @return DeployableRecord
     * @throws \Exception
     */
    protected function buildModel(array $row) : DeployableRecord
    {
        $model = new DeployableRecord(
            $row['identifier'],
            $row['table'],
            (int) $row['uid'],
            !empty($row['crdate']) ? (new \DateTime())->setTimestamp((int) $row['uid']) : null
        );

        return $model;
    }

    /**
     * @param DeployableRecord $deployableRecord
     * @return bool
     */
    public function add(DeployableRecord $deployableRecord) : bool
    {
        $result = $this->getQueryBuilder()->insert(self::TABLE)->values($deployableRecord->toArray())->execute();
        return (bool) $result;
    }

    /**
     * @param DeployableRecord $deployableRecord
     * @return bool
     */
    public function remove(DeployableRecord $deployableRecord) : bool
    {
        $queryBuilder = $this->getQueryBuilder();
        $result = $queryBuilder->delete(self::TABLE)->where(
            $queryBuilder->expr()->eq(
                'identifier',
                $queryBuilder->createNamedParameter($deployableRecord->getIdentifier())
            )
        )->execute();
        // TODO Children?
        return (bool) $result;
    }

    /**
     * @param array $filteredRow
     * @param string $table
     * @param array $fullDatabaseRow
     * @return array
     * @throws DumpException
     * @throws DumpReferenceException
     */
    public function resolveReferringFields(array $filteredRow, string $table, array $fullDatabaseRow) : array
    {
        $missingRelationsWithFieldName = [];
        foreach ($filteredRow as $column => $value) {
            $referenceTableNames = ReferringFieldHelper::getReferencedTableNames($table, $column);
            if (empty($referenceTableNames) || $value === null) {
                continue;
            }

            if (reset($referenceTableNames) === 'flexform') {
                try {
                    /** @var FlexFormTools $flexformTools */
                    $flexformTools = GeneralUtility::makeInstance(FlexFormTools::class);
                    $dsIdentifier = $flexformTools->getDataStructureIdentifier(
                        $GLOBALS['TCA'][$table]['columns'][$column],
                        $table,
                        $column,
                        $fullDatabaseRow
                    );
                    $filteredRow[$column] = ReferringFieldHelper::resolveFlexform(
                        $value,
                        $table,
                        $column,
                        $flexformTools->parseDataStructureByIdentifier($dsIdentifier)
                    );
                } catch (\TYPO3\CMS\Core\Configuration\FlexForm\Exception\AbstractInvalidDataStructureException $e) {
                    throw new DumpException(\get_class($e) . ': ' . $e->getMessage(), $e->getCode(), $e);
                }
                continue;
            }

            try {
                if (ReferringFieldHelper::getManyToManyTablename($table, $column)) {
                    /** @var AnyRecordRepository $anyRecordRepository */
                    $anyRecordRepository = GeneralUtility::makeInstance(AnyRecordRepository::class);
                    $value = $anyRecordRepository->findManyToManyRelations(
                        (int) $fullDatabaseRow['uid'],
                        $table,
                        $column
                    );
                }
                $resolvedValues = ReferringFieldHelper::resolveValues($value, $referenceTableNames);
            } catch (DumpReferenceException $exception) {
                foreach ($exception->getMissingRelations() as $missingRelation) {
                    $missingRelationsWithFieldName[$column][] = $missingRelation;
                }
                if (isset($missingRelationsWithFieldName[$column]) && !empty($missingRelationsWithFieldName[$column])) {
                    continue;
                }
            }
            if (!empty($resolvedValues)) {
                $filteredRow[$column] = implode(',', $resolvedValues);
            }
        }
        if (!empty($missingRelationsWithFieldName)) {
            throw new DumpReferenceException(
                'The record ' . $table . ':' . $filteredRow['uid'] . ' has one ore more references, ' .
                'which are not staged as deployable record.',
                0,
                null,
                $missingRelationsWithFieldName
            );
        }
        return $filteredRow;
    }

    /**
     * @param string $identifier
     * @param array $filteredRow
     * @param string $table
     * @return array
     * @throws DumpException
     */
    public function unresolveReferringFields(string $identifier, array $filteredRow, string $table) : array
    {
        foreach ($filteredRow as $column => $value) {
            $referenceTableNames = ReferringFieldHelper::getReferencedTableNames($table, $column);
            if (empty($referenceTableNames) || $value === null) {
                continue;
            }

            $row = $filteredRow;
            $deployableRecord = $this->findByIdentifier($identifier);
            if ($deployableRecord) {
                $row = $deployableRecord->getDatabaseRow();
            }

            if (reset($referenceTableNames) === 'flexform') {
                try {
                    /** @var FlexFormTools $flexformTools */
                    $flexformTools = GeneralUtility::makeInstance(FlexFormTools::class);
                    $dsIdentifier = $flexformTools->getDataStructureIdentifier(
                        $GLOBALS['TCA'][$table]['columns'][$column],
                        $table,
                        $column,
                        $row
                    );

                    $filteredRow[$column] = ReferringFieldHelper::unresolveFlexform(
                        $value,
                        $table,
                        $column,
                        $flexformTools->parseDataStructureByIdentifier($dsIdentifier)
                    );
                } catch (\TYPO3\CMS\Core\Configuration\FlexForm\Exception\AbstractInvalidDataStructureException $e) {
                    throw new DumpException(\get_class($e) . ': ' . $e->getMessage(), $e->getCode(), $e);
                }
                continue;
            }

            $unresolvedValues = ReferringFieldHelper::unresolveValues($value, $referenceTableNames);
            if (!empty($unresolvedValues)) {
                $filteredRow[$column] = implode(',', $unresolvedValues);
            }

            if (isset($filteredRow[$column]) &&
                $mmTable = ReferringFieldHelper::getManyToManyTablename($table, $column)
            ) {
                $filteredRow[$column] = [
                    'MM' => $mmTable,
                    'foreignTable' => $GLOBALS['TCA'][$table]['columns'][$column]['config']['foreign_table'],
                    'tablenames' => $table,
                    'fieldname' => $column,
                    'resolved' => $value,
                    'values' => $filteredRow[$column]
                ];
            }
        }
        return $filteredRow;
    }

    /**
     * @return \TYPO3\CMS\Core\Database\Query\QueryBuilder
     */
    private function getQueryBuilder()
    {
        return clone $this->queryBuilder;
    }
}
