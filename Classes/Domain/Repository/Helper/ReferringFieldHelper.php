<?php declare(strict_types=1);
namespace T3\DeployableRecords\Domain\Repository\Helper;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Configuration;
use T3\DeployableRecords\Domain\Repository\DeployableRecordRepository;
use T3\DeployableRecords\Dump\DumpException;
use T3\DeployableRecords\Dump\DumpReferenceException;
use TYPO3\CMS\Core\Configuration\FlexForm\FlexFormTools;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ReferringFieldHelper
 */
class ReferringFieldHelper
{
    /**
     * @var Configuration
     */
    private static $config;

    /**
     * @param string $table
     * @param string $column
     * @return array
     */
    public static function getReferencedTableNames(string $table, string $column) : array
    {
        if ($column === 'pid') {
            return ['pages'];
        }

        if (!static::$config) {
            static::$config = GeneralUtility::makeInstance(Configuration::class);
        }
        $fieldTsConfig = static::$config->get('tx_deployable_records.tables.' . $table. '.' . $column);
        if (isset($fieldTsConfig['resolve'])) {
            return GeneralUtility::trimExplode(',', $fieldTsConfig['resolve'], true);
        }

        $tcaConfig = $GLOBALS['TCA'][$table]['columns'][$column]['config'];
        if ($column === 'uid' || !\is_array($tcaConfig)) {
            return [];
        }
        if ($tcaConfig['type'] === 'flex') {
            return ['flexform'];
        }
        return static::guessReferenceTablesByTcaConfig($tcaConfig);
    }

    /**
     * @param string $table
     * @param string $column
     * @return string
     */
    public static function getManyToManyTablename(string $table, string $column) : string
    {
        $tcaConfig = $GLOBALS['TCA'][$table]['columns'][$column]['config'];
        return !isset($tcaConfig['MM_oppositeUsage']) && $tcaConfig['MM'] ? $tcaConfig['MM'] : '';
    }

    /**
     * @param int|string $referenceSlug
     * @param string[] $tableNames
     * @return array uid (int) and table (string).
     */
    public static function extractTableAndUidFromReferenceSlug($referenceSlug, array $tableNames) : array
    {
        $refUid = (int) $referenceSlug;
        $refTable = current($tableNames);
        if (\count($tableNames) > 1) {
            $parts = explode('_', $referenceSlug);
            $refUid = (int) array_pop($parts);
            $refTable = implode('_', $parts);
            if (!\in_array($refTable, $tableNames, true)) {
                return [];
            }
        }
        if ($refTable === 'pages' && $refUid === 0) {
            return [];
        }
        return [$refUid, $refTable];
    }

    /**
     * @param array|string|null $value eg. 5,87 or tt_content_5,pages_87 or array with uids
     * @param string[] $referenceTableNames
     * @return array
     * @throws DumpReferenceException
     */
    public static function resolveValues($value = null, array $referenceTableNames = []) : array
    {
        if ($value === null) {
            return [];
        }

        /** @var DeployableRecordRepository $deployableRecordRepository */
        $deployableRecordRepository = GeneralUtility::makeInstance(DeployableRecordRepository::class);

        if (!\is_array($value)) {
            $value = GeneralUtility::trimExplode(',', $value, true);
        }
        $resolvedValues = [];
        $missingRelations = [];
        // $value may be a comma separated list (1,2,3) or (tt_content_1,pages_1)
        foreach ($value as $referenceSlug) {
            list($refUid, $refTable) = static::extractTableAndUidFromReferenceSlug(
                $referenceSlug,
                $referenceTableNames
            );
            if (!$refTable || !$refUid || $refUid <= 0) {
                continue;
            }
            $deployableRecord = $deployableRecordRepository->findOneByTableAndUid($refTable, $refUid);
            if (!$deployableRecord) {
                $missingRelations[] = ['table' => $refTable, 'uid' => $refUid];
                continue;
            }
            $res = $deployableRecord->getIdentifier();
            $resolvedValues[] = $res;
        }
        if (!empty($missingRelations)) {
            throw new DumpReferenceException(
                'One or more references are not marked as deployable records!',
                0,
                null,
                $missingRelations
            );
        }
        return $resolvedValues;
    }

    /**
     * @param string|null $value
     * @param array $referenceTableNames
     * @return array
     */
    public static function unresolveValues(string $value = null, array $referenceTableNames = []) : array
    {
        if ($value === null) {
            return [];
        }

        /** @var DeployableRecordRepository $deployableRecordRepository */
        $deployableRecordRepository = GeneralUtility::makeInstance(DeployableRecordRepository::class);

        $unresolvedValues = [];
        // Converts identifiers to comma separated lists. Example: (1,2,3) or (tt_content_1,pages_1)
        foreach (GeneralUtility::trimExplode(',', $value, true) as $referenceIdentifier) {
            // Skip pid 0
            if ($referenceIdentifier === '0') {
                continue;
            }
            $deployableRecord = $deployableRecordRepository->findByIdentifier($referenceIdentifier);
            if ($deployableRecord) {
                $unresolvedValues[] = \count($referenceTableNames) > 1
                    ? $deployableRecord->getTable() . '_' . $deployableRecord->getUid()
                    : $deployableRecord->getUid();
            }
        }
        return $unresolvedValues;
    }

    /**
     * @param array $tcaConfig config node of certain field in TCA table
     * @return array Empty if the field is not a reference. Otherwise contains an array of the referenced table(s)
     */
    private static function guessReferenceTablesByTcaConfig(array $tcaConfig) : array
    {
        if ($tcaConfig['type'] === 'group') {
            return GeneralUtility::trimExplode(',', $tcaConfig['allowed'], true);
        }
        if (isset($tcaConfig['foreign_table'])
            && ($tcaConfig['type'] === 'select' || $tcaConfig['type'] === 'inline')
        ) {
            return [$tcaConfig['foreign_table']];
        }
        return [];
    }

    /**
     * @param string $value
     * @param string $table
     * @param string $column
     * @param array $ds
     * @return string
     * @throws DumpException
     */
    public static function resolveFlexform(string $value, string $table, string $column, array $ds) : string
    {
        $flexformValues = GeneralUtility::xml2array($value);
        // Sheets
        foreach ($ds['sheets'] as $sheetKey => $sheet) {
            $elements = $sheet['ROOT']['el'];
            // Fields
            if (!$elements) {
                continue;
            }
            foreach ($elements as $elementKey => $element) {
                // TODO: 'lDEF' or 'vDEF' can be different (translations)?
                $elementValue = $flexformValues['data'][$sheetKey]['lDEF'][$elementKey]['vDEF'];
                $referenceTableNames = static::guessReferenceTablesByTcaConfig($element['TCEforms']['config']);
                if (empty($referenceTableNames) || $elementValue === null) {
                    continue;
                }

                $resolvedValues = static::resolveValues($elementValue, $referenceTableNames);
                if (!empty($resolvedValues)) {
                    $flexformValues['data'][$sheetKey]['lDEF'][$elementKey]['vDEF'] = $resolvedValues;
                }
            }
        }
        /** @var FlexFormTools $flexformTools */
        $flexformTools = GeneralUtility::makeInstance(FlexFormTools::class);
        return $flexformTools->flexArray2Xml($flexformValues);
    }


    public static function unresolveFlexform(string $value, string $table, string $column, array $ds) : string
    {
        $flexformValues = GeneralUtility::xml2array($value);
        // Sheets
        foreach ($ds['sheets'] as $sheetKey => $sheet) {
            $elements = $sheet['ROOT']['el'];
            // Fields
            if (!$elements) {
                continue;
            }
            foreach ($elements as $elementKey => $element) {
                // TODO: 'lDEF' or 'vDEF' can be different (translations)?
                $elementValue = $flexformValues['data'][$sheetKey]['lDEF'][$elementKey]['vDEF'];

                $referenceTableNames = static::guessReferenceTablesByTcaConfig($element['TCEforms']['config']);
                if (empty($referenceTableNames) || empty($elementValue)) {
                    continue;
                }

                if (\is_array($elementValue)) {
                    $elementValue = implode(',', $elementValue);
                }
                $unresolvedValues = static::unresolveValues($elementValue, $referenceTableNames);
                if (!empty($unresolvedValues)) {
                    $flexformValues['data'][$sheetKey]['lDEF'][$elementKey]['vDEF'] = $unresolvedValues;
                }
            }
        }
        /** @var FlexFormTools $flexformTools */
        $flexformTools = GeneralUtility::makeInstance(FlexFormTools::class);
        return $flexformTools->flexArray2Xml($flexformValues);
    }
}
