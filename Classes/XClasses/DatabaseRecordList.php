<?php declare(strict_types=1);
namespace T3\DeployableRecords\XClasses;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Domain\Repository\DeployableRecordRepository;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class DatabaseRecordList
 */
class DatabaseRecordList extends \TYPO3\CMS\Recordlist\RecordList\DatabaseRecordList
{
    /**
     * Rendering a single row for the list
     *
     * @param string $table Table name
     * @param mixed[] $row Current record
     * @param int $cc Counter, counting for each time an element is rendered (used for alternating colors)
     * @param string $titleCol Table field (column) where header value is found
     * @param string $thumbsCol Table field (column) where (possible) thumbnails can be found
     * @param int $indent Indent from left.
     * @return string Table row for the element
     * @access private
     * @see getTable()
     */
    public function renderListRow($table, $row, $cc, $titleCol, $thumbsCol, $indent = 0)
    {
        $rowOutput = parent::renderListRow($table, $row, $cc, $titleCol, $thumbsCol, $indent);
        $deployableRecordRepository = GeneralUtility::makeInstance(DeployableRecordRepository::class);
        if ($deployableRecordRepository->findOneByTableAndUid($table, (int) $row['uid'])) {
            $this->addJsAndCssToPageRenderer();
            return preg_replace('/t3js-entity/', 't3js-entity deployable-record', $rowOutput, 1);
        }
        return $rowOutput;
    }

    /**
     * Add RequireJS module and CSS file to PageRenderer
     *
     * @return void
     */
    protected function addJsAndCssToPageRenderer()
    {
        /** @var PageRenderer $pageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->loadRequireJsModule('TYPO3/CMS/DeployableRecords/DeployableRecords');
        $pageRenderer->addCssFile('EXT:deployable_records/Resources/Public/Styles/ListView.css');
    }
}
