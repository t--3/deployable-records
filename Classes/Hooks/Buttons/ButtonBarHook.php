<?php declare(strict_types=1);
namespace T3\DeployableRecords\Hooks\Buttons;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Button Hook
 *
 * @see \TYPO3\CMS\Backend\Template\Components\ButtonBar
 */
class ButtonBarHook
{
    use ButtonHelperTrait;

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function add(array $params)
    {
        $table = 'pages';
        $uid = (int) GeneralUtility::_GET('id');
        if ($uid === 0) {
            // Check for edit record view
            $edit = GeneralUtility::_GET('edit') ?: [];
            if (!empty($edit)) {
                $keys = array_keys($edit);
                $table = reset($keys);
                $keys2 = array_keys($edit[$table]);
                $uid = (int) reset($keys2);
            }
        }
        if ($uid === 0) {
            return $params['buttons'];
        }

        /** @var PageRenderer $pageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->loadRequireJsModule('TYPO3/CMS/DeployableRecords/DeployableRecords');
        $pageRenderer->addCssFile('EXT:deployable_records/Resources/Public/Styles/ListView.css');

        $params['buttons']['left'][] = [$this->makeLinkButton($uid, $table)];
        return $params['buttons'];
    }
}
