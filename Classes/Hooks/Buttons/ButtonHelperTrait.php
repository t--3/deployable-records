<?php declare(strict_types=1);
namespace T3\DeployableRecords\Hooks\Buttons;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */

use T3\DeployableRecords\Domain\Model\DeployableRecord;
use T3\DeployableRecords\Domain\Repository\DeployableRecordRepository;
use TYPO3\CMS\Backend\Template\Components\Buttons\LinkButton;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Helper trait used in several hook classes
 */
trait ButtonHelperTrait
{
    /**
     * @return bool
     */
    protected function isAdmin() : bool
    {
        return $GLOBALS['BE_USER']->isAdmin();
    }

    /**
     * Returns the icon for list row button
     *
     * @param string $iconIdentifier Optional
     * @return Icon
     */
    protected function getButtonIcon($iconIdentifier = 'ext-deployable-records-icon')
    {
        $iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        $icon = $iconFactory->getIcon(
            $iconIdentifier,
            Icon::SIZE_SMALL
        );
        return $icon;
    }

    /**
     * @param string $table
     * @param int $uid
     * @return DeployableRecord|null
     * @throws \Exception
     */
    protected function getDeployableRecord(string $table, int $uid)
    {
        /** @var DeployableRecordRepository $deployableRecordsRepository */
        $deployableRecordsRepository = GeneralUtility::makeInstance(DeployableRecordRepository::class);
        $deployableRecord = $deployableRecordsRepository->findOneByTableAndUid($table, $uid);
        return $deployableRecord;
    }

    /**
     * @param int $uid Current page id
     * @param string $table
     * @param string $class
     * @return LinkButton|ListLinkButton
     * @throws \Exception
     */
    protected function makeLinkButton(
        int $uid,
        string $table = 'pages',
        $class = LinkButton::class
    ) : LinkButton {
        /** @var LinkButton|ListLinkButton $button */
        $button = GeneralUtility::makeInstance($class);
        $button->setTitle('Add to deployable records (' . $table . ':' . $uid . ')');
        $button->setHref('#');
        $button->setShowLabelText(false);
        $button->setClasses('deployable-records-dump');
        $button->setDataAttributes(['table' => $table, 'uid' => $uid]);

        $deployableRecord = $this->getDeployableRecord($table, $uid);
        if ($deployableRecord) {
            $button->setTitle(
                'Edit deployable record (' . $table . ':' . $uid . ' -> ' . $deployableRecord->getIdentifier() . ')'
            );
            $button->setClasses($button->getClasses() . ' active');
            $button->setDataAttributes(array_merge($button->getDataAttributes(), [
                'identifier' => $deployableRecord->getIdentifier()
            ]));
        }

        /** @var IconFactory $iconFactory */
        $iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        $button->setIcon($iconFactory->getIcon('ext-deployable-records-icon', Icon::SIZE_SMALL));
        return $button;
    }
}
