<?php declare(strict_types=1);
namespace T3\DeployableRecords\Hooks\Buttons;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * Class ListLinkButton
 */
class ListLinkButton extends \TYPO3\CMS\Backend\Template\Components\Buttons\LinkButton
{
    /**
     * @return string
     */
    public function render()
    {
        return str_replace('btn-sm', '', parent::render());
    }
}
