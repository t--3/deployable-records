<?php declare(strict_types=1);
namespace T3\DeployableRecords\Hooks\Buttons;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Dump\DumpUtility;
use TYPO3\CMS\Recordlist\RecordList\RecordListHookInterface;

/**
 * Hook DumpButton
 * for $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['typo3/class.db_list_extra.inc']['actions']
 */
class ListRowButtonHook extends AbstractRecordListHook implements RecordListHookInterface
{
    use ButtonHelperTrait;

    /**
     * Adds dump (or edit) button in list row, if current table is configured in PageTS
     *
     * @param string $table The current database table
     * @param array $row The current record row
     * @param array $cells The default clip-icons to get modified
     * @param object $parentObject Instance of calling object
     * @return array The modified clip-icons
     * @throws \Exception
     */
    public function makeClip($table, $row, $cells, &$parentObject)
    {
        if ($this->isAdmin()) {
            // TODO: Check if dump file is properly configured
            if (!DumpUtility::tableIsAllowed($table, $row['pid'])) {
                // Early return, when current table is not configured in PageTS (tx_deployable_records.tables)
                // TODO What happens with records which are already staged and disabled? Edit button will not appear.
                return $cells;
            }

            // Variables used in link output
            $button = $this->makeLinkButton((int) $row['uid'], $table, ListLinkButton::class);
            $cells['deployable_records'] = $button->render();
        }
        return $cells;
    }
}
