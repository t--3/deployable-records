<?php declare(strict_types=1);
namespace T3\DeployableRecords\Hooks;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Domain\Repository\AnyRecordRepository;
use T3\DeployableRecords\Dump\DumpController;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Hook ProcessDatamap
 * for $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']
 */
class ProcessDatamap
{
    /**
     * @var AnyRecordRepository
     */
    protected $anyRecordRepository;

    /**
     * @var DumpController
     */
    protected $dumpController;


    public function __construct()
    {
        $this->anyRecordRepository = GeneralUtility::makeInstance(AnyRecordRepository::class);
        $this->dumpController = GeneralUtility::makeInstance(DumpController::class);
    }

    // phpcs:disable

    /**
     * Hook action
     *
     * @param $status
     * @param $table
     * @param $id
     * @param array $fieldArray
     * @param DataHandler $pObj
     * @return void
     * @throws \T3\DeployableRecords\Dump\DumpException
     * @throws \T3\DeployableRecords\Dump\IO\IOException
     * @throws \TYPO3\CMS\Core\Exception
     */
    public function processDatamap_afterDatabaseOperations(
        $status,
        $table,
        $id,
        array $fieldArray,
        DataHandler $pObj
    ) {
        $uid = $this->getUid($id, $table, $status, $pObj);
        $record = $this->getRecord($table, $uid);

        if ($this->dumpController->isAutoDumpEnabled($record['pid'])) {
            $deployableRecord = $this->dumpController->getDeployableRecordByTableAndUid($table, $uid);
            if ($deployableRecord) {

                $dumpStatus = $this->dumpController->getDumpStatus($deployableRecord);
                if (!$dumpStatus->isInSync()) {
                    $status = $this->dumpController->updateDeployableRecord(
                        DumpController::FROM_DB_TO_DUMP,
                        $table,
                        $deployableRecord->getIdentifier()
                    );

                    /** @var $flashMessageService FlashMessageService */
                    $flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class);
                    $flashMessageQueue = $flashMessageService->getMessageQueueByIdentifier();

                    /** @var FlashMessage $flashMessage */
                    $flashMessage = GeneralUtility::makeInstance(
                        FlashMessage::class,
                        $status ? 'The dump file has been automatically updated for this record with identifier "' .
                                  $deployableRecord->getIdentifier() . '".'
                                : 'An error occurred while writing updates to dump file!',
                        'Deployable Record Auto Dump',
                        $status ? FlashMessage::OK : FlashMessage::ERROR,
                        true
                    );
                    $flashMessageQueue->enqueue($flashMessage);
                }
            }
        }
    }

    // phpcs:enable

    /**
     * @param string $table
     * @param int $uid
     * @return array
     */
    protected function getRecord(string $table, int $uid) : array
    {
        return $this->anyRecordRepository->findByTableAndUid($table, $uid);
    }

    /**
     * Investigates the uid of entry
     *
     * @param int|string $id
     * @param string $table
     * @param string $status
     * @param DataHandler $pObj
     * @return int
     */
    protected function getUid($id, string $table, string $status, DataHandler $pObj) : int
    {
        $uid = $id;
        if ($status === 'new') {
            if (!$pObj->substNEWwithIDs[$id]) {
                //postProcessFieldArray
                $uid = 0;
            } else {
                //afterDatabaseOperations
                $uid = $pObj->substNEWwithIDs[$id];
                if (isset($pObj->autoVersionIdMap[$table][$uid])) {
                    $uid = $pObj->autoVersionIdMap[$table][$uid];
                }
            }
        }
        return $uid;
    }
}
