<?php declare(strict_types=1);
namespace T3\DeployableRecords\Hooks;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Configuration;
use T3\DeployableRecords\Dump\DumpController;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Hook TemplateService
 * for $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['Core/TypoScript/TemplateService']['runThroughTemplatesPostProcessing']
 */
class TemplateService
{
    /**
     * Registers constants for all deployable record identifiers in TypoScript, given by the system.
     */
    public function runThroughTemplatesPostProcessing()
    {
        $config = GeneralUtility::makeInstance(Configuration::class);
        $dumpController = GeneralUtility::makeInstance(DumpController::class);
        $constants = [];
        try {
            /** @var \T3\DeployableRecords\Dump\DumpStatus $dumpStatus */
            foreach ($dumpController->getStatusForAll() as $dumpStatus) {
                $constants[] = $config->get('tx_deployable_records.typoscriptConstantPrefix', 'pid.') .
                    $dumpStatus->getIdentifier() . ' = ' . $dumpStatus->getRow()['uid'];
            }
            ExtensionManagementUtility::addTypoScriptConstants(implode(PHP_EOL, $constants));
        } catch (\Exception $e) {
        }
    }
}
