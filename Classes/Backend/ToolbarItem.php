<?php declare(strict_types=1);
namespace T3\DeployableRecords\Backend;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Dump\DumpController;
use TYPO3\CMS\Backend\Domain\Repository\Module\BackendModuleRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Backend\Toolbar\ToolbarItemInterface;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * Class to render the status panel for Deployable Records
 */
class ToolbarItem implements ToolbarItemInterface
{
    /**
     * @var DumpController|null
     */
    private $status;

    /**
     * Constructor
     */
    public function __construct()
    {
        try {
            $this->status = (GeneralUtility::makeInstance(DumpController::class))->getStatusForAll();
        } catch (\T3\DeployableRecords\Dump\DumpException $exception) {
        }
    }

    /**
     * Checks whether the user has access to this toolbar item,
     * only allowed when administrator.
     *
     * @return bool TRUE if user has access, FALSE if not
     */
    public function checkAccess()
    {
        return $GLOBALS['BE_USER']->isAdmin();
    }

    /**
     * Render search field
     *
     * @return string Live search form HTML
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\InvalidExtensionNameException
     */
    public function getItem()
    {
        return $this->getFluidTemplateObject()->renderSection('Item');
    }

    /**
     * This item needs to additional attributes
     *
     * @return array
     * @throws \T3\DeployableRecords\Dump\DumpException
     */
    public function getAdditionalAttributes()
    {
        if ($this->status && $this->status->hasErrors()) {
            return ['class' => 'toolbar-item-deployable-records has-errors'];
        }
        if ($this->status && !$this->status->isAllInSync()) {
            return ['class' => 'toolbar-item-deployable-records not-in-sync'];
        }
        return ['class' => 'toolbar-item-deployable-records in-sync'];
    }

    /**
     * This item has no drop down
     *
     * @return bool
     */
    public function hasDropDown()
    {
        return true;
    }

    /**
     * Render DropDown
     *
     * @return string
     */
    public function getDropDown()
    {
        return $this->getFluidTemplateObject()->renderSection('DropDown', ['status' => $this->status]);
    }

    /**
     * Position relative to others, live search should be very right
     *
     * @return int
     */
    public function getIndex()
    {
        return 10;
    }

    /**
     * Returns current PageRenderer
     *
     * @return PageRenderer
     */
    protected function getPageRenderer()
    {
        return GeneralUtility::makeInstance(PageRenderer::class);
    }

    /**
     * Returns a new standalone view, shorthand function
     *
     * @return StandaloneView
     */
    protected function getFluidTemplateObject() : StandaloneView
    {
        $view = GeneralUtility::makeInstance(StandaloneView::class);
        $view->setLayoutRootPaths(['EXT:backend/Resources/Private/Layouts']);
        $view->setPartialRootPaths([
            'EXT:backend/Resources/Private/Partials/ToolbarItems',
            'EXT:deployable_records/Resources/Private/Partials'
        ]);
        $view->setTemplatePathAndFilename('EXT:deployable_records/Resources/Private/Templates/ToolbarItem.html');
        return $view;
    }
}
