<?php declare(strict_types=1);
namespace T3\DeployableRecords\Dump;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * Class DumpStatusCollection
 */
final class DumpStatusCollection extends \Doctrine\Common\Collections\ArrayCollection
{
    /**
     * @return DumpStatus[]
     * @throws DumpException
     */
    public function getRecordsNewInDatabase() : array
    {
        $new = [];
        /** @var DumpStatus $dumpStatus */
        foreach ($this as $dumpStatus) {
            if ($dumpStatus->isNew()) {
                $new[] = $dumpStatus;
            }
        }
        return $this->orderRecordsByTable($new);
    }

    /**
     * @return DumpStatus[]
     * @throws DumpException
     */
    public function getRecordsMissingInDump() : array
    {
        $missing = [];
        /** @var DumpStatus $dumpStatus */
        foreach ($this as $dumpStatus) {
            if ($dumpStatus->isMissing()) {
                $missing[] = $dumpStatus;
            }
        }
        return $this->orderRecordsByTable($missing);
    }

    /**
     * @return DumpStatus[]
     */
    public function getRecordsOutOfSync() : array
    {
        $outOfSync = [];
        /** @var DumpStatus $dumpStatus */
        foreach ($this as $dumpStatus) {
            if (!$dumpStatus->isInSync()) {
                $outOfSync[] = $dumpStatus;
            }
        }
        return $this->orderRecordsByTable($outOfSync);
    }

    /**
     * @return DumpStatus[]
     */
    public function getRecordsInSync() : array
    {
        $inSync = [];
        /** @var DumpStatus $dumpStatus */
        foreach ($this as $dumpStatus) {
            if ($dumpStatus->isInSync()) {
                $inSync[] = $dumpStatus;
            }
        }
        return $this->orderRecordsByTable($inSync);
    }

    /**
     * @return DumpStatus[]
     */
    public function getRecordsWithErrors() : array
    {
        $hasErrors = [];
        /** @var DumpStatus $dumpStatus */
        foreach ($this as $dumpStatus) {
            if ($dumpStatus->hasErrors()) {
                $hasErrors[] = $dumpStatus;
            }
        }
        return $hasErrors;
    }


    /**
     * Checks if all DumpStatus entries in collection, are in sync
     *
     * @return bool
     */
    public function isAllInSync() : bool
    {
        /** @var DumpStatus $dumpStatus */
        foreach ($this as $dumpStatus) {
            if (!$dumpStatus->isInSync()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if any DumpStatus given, has errors
     *
     * @return bool
     */
    public function hasErrors() : bool
    {
        /** @var DumpStatus $dumpStatus */
        foreach ($this as $dumpStatus) {
            if ($dumpStatus->hasErrors()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Alias for $this->hasErrors()
     *
     * @return bool
     */
    public function isHasErrors() : bool
    {
        return $this->hasErrors();
    }

    /**
     * Orders records by table priority
     *
     * @param DumpStatus[] $records
     * @return DumpStatus[]
     * @TODO Do not hardcode table names. Check TCA config for best table order during sync
     */
    private function orderRecordsByTable(array $records) : array
    {
        $groupedRecordsByPriority = [1 => [], 2 => [], 3 => []];
        foreach ($records as $record) {
            switch ($record->getTable()) {
                case 'sys_category':
                    $groupedRecordsByPriority[1][] = $record;
                    break;

                default:
                    $groupedRecordsByPriority[2][] = $record;
                    break;

                case 'pages':
                    $groupedRecordsByPriority[3][] = $record;
                    break;
            }
        }
        return array_merge($groupedRecordsByPriority[1], $groupedRecordsByPriority[2], $groupedRecordsByPriority[3]);
    }
}
