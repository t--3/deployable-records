<?php declare(strict_types=1);
namespace T3\DeployableRecords\Dump;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Domain\Model\DeployableRecord;
use TYPO3\CMS\Backend\Utility\BackendUtility;

/**
 * Class RecordStatus
 */
class DumpStatus
{
    const STATUS_NEW = 'new';
    const STATUS_MISSING = 'missing';
    const STATUS_DIFF = 'diff';
    const STATUS_SYNC = 'sync';

    /**
     * @var string
     */
    private $table;

    /**
     * @var DeployableRecord
     */
    private $deployableRecord;

    /**
     * @var DumpOptions
     */
    private $options;

    /**
     * @var array
     */
    private $row;

    /**
     * @var array
     */
    private $dumpRow;

    /**
     * @var string
     */
    private $identifier;

    /**
     * @var \Exception[]
     */
    private $errors = [];

    /**
     * @var bool
     */
    private $validationProcessed = false;

    /**
     * @var array
     */
    private $cache = [];

    /**
     * DumpStatus constructor
     *
     * @param string $table
     * @param DeployableRecord|null $deployableRecord
     * @param array $row
     * @param array $dumpRow
     * @param string $identifier
     */
    public function __construct(
        string $table,
        DeployableRecord $deployableRecord = null,
        array $row = [],
        array $dumpRow = [],
        string $identifier = null
    ) {
        $this->table = $table;
        $this->deployableRecord = $deployableRecord;
        $this->row = $row;
        $this->dumpRow = $dumpRow;
        $this->identifier = $identifier ?? '';

        $this->options = new DumpOptions($this->dumpRow['__options__'] ?? []);
    }

    /**
     * @return string
     */
    public function getTable() : string
    {
        return $this->table;
    }

    /**
     * @return DeployableRecord|null
     */
    public function getDeployableRecord() : ?DeployableRecord
    {
        return $this->deployableRecord;
    }

    /**
     * @return DumpOptions
     */
    public function getOptions() : DumpOptions
    {
        return $this->options;
    }

    /**
     * @return array
     */
    public function getRow() : array
    {
        return $this->row;
    }

    /**
     * @return array
     */
    public function getDumpRow() : array
    {
        return $this->dumpRow;
    }

    /**
     * @return string
     */
    public function getIdentifier() : string
    {
        return $this->identifier;
    }

    /**
     * @return bool
     */
    public function isDeployableRecord() : bool
    {
        return $this->deployableRecord !== null;
    }

    /**
     * @return bool
     */
    public function dumpRowExists() : bool
    {
        return !empty($this->dumpRow);
    }

    /**
     * @return bool
     */
    public function getDumpRowExists() : bool
    {
        return $this->dumpRowExists();
    }

    /**
     * @return bool
     */
    public function rowExists() : bool
    {
        return !empty($this->row);
    }

    /**
     * @return bool
     * @throws DumpException is not thrown, because of false flag
     */
    private function parentRowIsStaged() : bool
    {
        return DumpUtility::checkIfParentPageIsStaged($this->row, false);
    }

    /**
     * @return \Exception[]
     */
    public function getErrors() : array
    {
        if (!$this->validationProcessed) {
            $this->getDiff();
        }
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function hasErrors() : bool
    {
        return !empty($this->getErrors());
    }

    /**
     * Alias method for $this->hasErrors()
     *
     * @return bool
     */
    public function isHasErrors() : bool
    {
        return $this->hasErrors();
    }

    /**
     * @return bool
     */
    public function isInSync() : bool
    {
        return empty($this->getDiff());
    }

    /**
     * @return array
     */
    public function getDiff() : array
    {
        $differences = [];
        try {
            $databaseRow = $this->getPreparedDatabaseRow();
            $dumpRow = $this->getPreparedDumpRow();
            foreach (array_merge_recursive($dumpRow, $databaseRow) as $key => $value) {
                if (!\is_array($value)) {
                    if (is_string($value)) {
                        $value = trim($value);
                    }
                    if (array_key_exists($key, $this->row) && !array_key_exists($key, $this->dumpRow)) {
                        $diff = ['db' => $value, 'dump' => null];
                    } elseif (!array_key_exists($key, $this->row) && array_key_exists($key, $this->dumpRow)) {
                        $diff = ['db' => null, 'dump' => $value];
                    } else {
                        $diff = ['db' => $value, 'dump' => $value];
                    }
                } else {
                    $dbValue = is_string($value[1]) ? trim($value[1]) : $value[1];
                    $dumpValue = is_string($value[0]) ? trim($value[0]) : $value[0];
                    $diff = [
                        'db' => $dbValue,
                        'dump' => $dumpValue
                    ];
                }
                if ($diff['db'] !== $diff['dump']) {
                    $differences[$key] = $diff;
                }
            }
        } catch (\Exception $exception) {
            if (!$this->validationProcessed) {
                $this->errors[] = $exception;
            }
        }
        $this->validationProcessed = true;
        return $differences;
    }

    /**
     * @return array
     * @throws DumpException
     * @throws DumpReferenceException
     */
    private function getPreparedDatabaseRow() : array
    {
        $row = DumpUtility::prepareDatabaseRow($this->row, $this->table);
        unset($row['__table__']);
        return $row;
    }

    /**
     * @return array
     * @throws DumpException
     */
    private function getPreparedDumpRow() : array
    {
        $dumpRow = [];
        if ($this->dumpRow) {
            $dumpRow = DumpUtility::prepareDumpRow($this->identifier, $this->dumpRow, $this->table, false);
            foreach ($dumpRow as $key => $value) {
                if (is_array($value) && array_key_exists('MM', $value)) {
                    $dumpRow[$key] = $value['resolved'];
                }
            }
        }
        return $dumpRow;
    }

    /**
     * @return string
     */
    public function getStatus() : string
    {
        if (!$this->rowExists()) {
            return static::STATUS_NEW;
        }
        if (!$this->dumpRowExists()) {
            return static::STATUS_MISSING;
        }
        return $this->isInSync() ? static::STATUS_SYNC : static::STATUS_DIFF;
    }

    /**
     * @return bool
     */
    public function isNew() : bool
    {
        return $this->getStatus() === static::STATUS_NEW;
    }

    /**
     * @return bool
     */
    public function isMissing() : bool
    {
        return $this->getStatus() === static::STATUS_MISSING;
    }

    /**
     * @return array
     * @throws DumpException
     */
    public function getMissingRelations() : array
    {
        try {
            DumpUtility::prepareDatabaseRow($this->getRow(), $this->getTable());
        } catch (DumpReferenceException $exception) {
            return $exception->getMissingRelations();
        }
        return [];
    }

    /**
     * Returns a suggestion for the identifier string, based on table name and TCA label value.
     * e.g. "pages_my_homepage"
     *
     * @return string
     */
    public function getIdentifierSuggestion() : string
    {
        $suggestion = '';
        if ($this->rowExists() && !$this->isDeployableRecord()) {
            $suggestion = $this->getTable() . '_' . DumpUtility::sanitizeIdentifier(
                BackendUtility::getRecordTitle($this->getTable(), $this->getRow())
            );
        }
        return $suggestion;
    }

    /**
     * Combines all getter of this DumpStatus to one output
     *
     * @return array
     * @throws DumpException in parentRowIsStaged method is not thrown, because of false flag
     */
    public function toArray() : array
    {
        if (empty($this->cache)) {
            $this->cache = [
                'status' => (string) $this,
                'isDeployableRecord' => $this->isDeployableRecord(),
                'dumpRowExists' => $this->dumpRowExists(),
                'rowExists' => $this->rowExists(),
                'parentRowIsStaged' => $this->parentRowIsStaged(),
                'deployableRecord' => $this->deployableRecord ? [
                    'identifier' => $this->deployableRecord->getIdentifier(),
                    'table' => $this->deployableRecord->getTable(),
                    'uid' => $this->deployableRecord->getUid()
                ] : null,
                'table' => $this->getTable(),
                'row' => $this->getRow(),
                'dumpRow' => $this->getDumpRow(),
                'options' => $this->getOptions()->toArray(),
                'isInSync' => $this->isInSync(),
                'missingRelations' => $this->getMissingRelations(),
                'diff' => !$this->isInSync() ? $this->getDiff() : [],
                'identifierSuggestion' => $this->getIdentifierSuggestion()
            ];
        }
        return $this->cache;
    }

    /**
     * Alias method
     *
     * @return array
     * @throws DumpException
     */
    public function getToArray() : array
    {
        return $this->toArray();
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->getStatus();
    }
}
