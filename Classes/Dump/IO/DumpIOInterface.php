<?php declare(strict_types=1);
namespace T3\DeployableRecords\Dump\IO;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Dump\DumpOptions;

/**
 * Interface IOInterface
 */
interface DumpIOInterface
{
    /**
     * IOInterface constructor
     *
     * @param string $filePath
     * @param array $options
     */
    public function __construct(string $filePath, array $options = []);

    /**
     * @return string
     */
    public function getFilePath() : string;

    /**
     * @param string $identifier
     * @param array $row
     * @param DumpOptions|null $options
     * @return bool It is recommended to throw IOException on error
     */
    public function writeRow(string $identifier, array $row, DumpOptions $options = null) : bool;

    /**
     * @param string $identifier
     * @return array
     */
    public function getRow(string $identifier) : array;

    /**
     * @return array
     */
    public function getAll() : array;
}
