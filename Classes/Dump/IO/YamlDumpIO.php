<?php declare(strict_types=1);
namespace T3\DeployableRecords\Dump\IO;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use Symfony\Component\Yaml\Yaml;

/**
 * Class YamlDumpIO
 */
class YamlDumpIO extends AbstractDumpIO
{
    public function __construct(string $filePath, array $options = [])
    {
        // Define option defaults
        $options = array_merge([
            'yamlInline' => 2,
            'yamlIndent' => 4,
            'yamlFlags' => Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK
        ], $options);
        parent::__construct($filePath, $options);
    }

    /**
     * @param mixed $content
     * @return bool
     * @throws IOException
     */
    protected function write($content) : bool
    {
        $yamlString = Yaml::dump(
            $content,
            (int) $this->options['yamlInline'],
            (int) $this->options['yamlIndent'],
            (int) $this->options['yamlFlags']
        );
        try {
            $status = (bool) file_put_contents($this->filePath, $yamlString);
        } catch (\Exception $exception) {
            throw new IOException($exception->getMessage(), $exception->getCode(), $exception);
        }
        return $status;
    }

    /**
     * @return bool False when file does not exist. If true it set $this->content to decoded json content
     * @throws IOException when json is not valid
     */
    protected function read() : bool
    {
        if (!parent::read()) {
            return false;
        }
        $yaml = Yaml::parse(file_get_contents($this->filePath), (int) $this->options['yamlFlags']);
        if (!$yaml) {
            throw new IOException('Unable to parse YAML from "' . $this->filePath . '".');
        }
        $this->content = $yaml;
        return true;
    }
}
