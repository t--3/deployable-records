<?php
namespace T3\DeployableRecords\Dump\IO;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Configuration;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class DumpIOFactory
 */
class DumpIOFactory
{
    /**
     * @var array
     */
    private static $cache = [];

    /**
     * @var Configuration|null
     */
    private static $config;

    /**
     * Creates (or returns cached) instance for dump IO.
     * The instance must extend from \T3\DeployableRecords\Dump\IO\AbstractDumpIO.
     *
     * @param int $pageUid
     * @return AbstractDumpIO
     */
    public static function get(int $pageUid = 0) : AbstractDumpIO
    {
        if (array_key_exists($pageUid, static::$cache)) {
            return static::$cache[$pageUid];
        }

        static::$config = static::$config ?? GeneralUtility::makeInstance(Configuration::class);
        static::$cache[$pageUid] = GeneralUtility::makeInstance(
            $dumpIOClassName = ltrim(
                static::$config->get(
                    'tx_deployable_records.dumpIO',
                    JsonDumpIO::class,
                    $pageUid
                ),
                '\\'
            ),
            GeneralUtility::getFileAbsFileName(
                static::$config->get('tx_deployable_records.output', '', $pageUid)
            ),
            static::$config->get(
                'tx_deployable_records.dumpIO.options',
                [],
                $pageUid
            )
        );

        if (!static::$cache[$pageUid] instanceof AbstractDumpIO) {
            throw new \UnexpectedValueException(
                'The configured class for dump IO "' . $dumpIOClassName . '" does not extend from ' .
                AbstractDumpIO::class . '.'
            );
        }
        return static::$cache[$pageUid];
    }
}
