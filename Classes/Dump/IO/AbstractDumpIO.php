<?php declare(strict_types=1);
namespace T3\DeployableRecords\Dump\IO;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Dump\DumpOptions;

/**
 * Abstract Class AbstractDumpIO
 */
abstract class AbstractDumpIO implements DumpIOInterface
{
    /**
     * @var string
     */
    protected $filePath;

    /**
     * @var array decoded json string
     */
    protected $content;

    /**
     * @var array
     */
    protected $options;

    /**
     * JsonDumpIO constructor
     *
     * @param string $filePath Absolute file path
     * @throws IOException
     */
    public function __construct(string $filePath, array $options = [])
    {
        if (empty($filePath)) {
            throw new IOException('No file path defined for dump!');
        }
        if (!is_writable(\dirname($filePath))) {
            throw new IOException('Path "' . $filePath . '" is not writable.');
        }
        $this->filePath = $filePath;
        $this->options = $options;
    }

    /**
     * Implement this method
     *
     * @param mixed $content
     * @return bool
     * @throws IOException
     */
    protected function write($content) : bool
    {
        throw new IOException('write() method not implemented!');
        return false;
    }

    /**
     * Implement this method
     *
     * @return bool False when file does not exist. If true it set $this->content to decoded json content
     */
    protected function read() : bool
    {
        if (\is_array($this->content)) {
            return true;
        }
        if (!file_exists($this->filePath)) {
            return false;
        }
        return true;
    }

    /**
     * @param string $identifier
     * @param array|null $row
     * @param DumpOptions|null $options
     * @return bool true on success, throws exception on failure
     * @throws IOException
     */
    public function writeRow(string $identifier, array $row = null, DumpOptions $options = null) : bool
    {
        $this->read();

        if (null === $row && array_key_exists($identifier, $this->content['rows'])) {
            unset($this->content['rows'][$identifier]);
        } else {
            if ($options) {
                $row['__options__'] = $options->toArray();
            }
            $this->content['rows'][$identifier] = $row;
        }

        $content = [
            'updated' => date('d.m.Y H:i:s'),
            'rows' => $this->content['rows']
        ];
        return $this->write($content);
    }

    /**
     * @param string $identifier If given, only the row with given identifier is returned.
     * @return array
     */
    public function getRow(string $identifier) : array
    {
        // $this->read() must be first!
        if ($this->read() && isset($this->content['rows']) && array_key_exists($identifier, $this->content['rows'])) {
            return $this->content['rows'][$identifier];
        }
        return [];
    }

    /**
     * @return array
     */
    public function getAll() : array
    {
        if ($this->read()) {
            return $this->content ?: [];
        }
        return [];
    }

    /**
     * @return string
     */
    public function getFilePath() : string
    {
        return $this->filePath;
    }
}
