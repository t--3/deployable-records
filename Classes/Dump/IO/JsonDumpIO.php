<?php declare(strict_types=1);
namespace T3\DeployableRecords\Dump\IO;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */

/**
 * Class JsonDumpIO
 */
final class JsonDumpIO extends AbstractDumpIO
{
    /**
     * JsonDumpIO constructor
     *
     * @param string $filePath
     * @param array $options
     * @throws IOException
     */
    public function __construct(string $filePath, array $options = [])
    {
        // Define option defaults
        $options = array_merge([
            'jsonEncodeOptions' => JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_LINE_TERMINATORS
        ], $options);
        parent::__construct($filePath, $options);
    }

    /**
     * @param mixed $content
     * @return bool
     * @throws IOException
     */
    protected function write($content) : bool
    {
        $jsonString = json_encode($content, (int) $this->options['jsonEncodeOptions']);
        try {
            $status = (bool) file_put_contents($this->filePath, $jsonString);
        } catch (\Exception $exception) {
            throw new IOException($exception->getMessage(), $exception->getCode(), $exception);
        }
        return $status;
    }

    /**
     * @return bool False when file does not exist. If true it set $this->content to decoded json content
     * @throws IOException when json is not valid
     */
    protected function read() : bool
    {
        if (!parent::read()) {
            return false;
        }
        $json = json_decode(file_get_contents($this->filePath), true);
        if (!$json) {
            throw new IOException('Unable to parse json from "' . $this->filePath . '".');
        }
        $this->content = $json;
        return true;
    }
}
