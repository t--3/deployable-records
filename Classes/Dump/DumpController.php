<?php declare(strict_types=1);
namespace T3\DeployableRecords\Dump;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Domain\Model\DeployableRecord;
use T3\DeployableRecords\Domain\Repository\AnyRecordRepository;
use T3\DeployableRecords\Domain\Repository\DeployableRecordRepository;
use T3\DeployableRecords\Configuration;
use T3\DeployableRecords\Dump\IO\IOException;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * The Dump Controller
 */
class DumpController
{
    const FROM_DUMP_TO_DB = 'db';
    const FROM_DB_TO_DUMP = 'dump';

    /**
     * @var int
     */
    protected $pageUid = 0;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @var array
     */
    private $dumpIOCache = [];

    /**
     * @var AnyRecordRepository
     */
    protected $anyRecordRepository;

    /**
     * @var DeployableRecordRepository
     */
    protected $deployableRecordRepository;

    /**
     * DumpController constructor
     *
     * @param int $pageUid
     * @throws \RuntimeException
     */
    public function __construct(int $pageUid = 0)
    {
        $this->pageUid = $pageUid;
        $this->config = GeneralUtility::makeInstance(Configuration::class);
        $this->deployableRecordRepository = GeneralUtility::makeInstance(DeployableRecordRepository::class);
        $this->anyRecordRepository = GeneralUtility::makeInstance(AnyRecordRepository::class);
    }

    /**
     * Checks AutoDump on global level (only)
     *
     * @param int|null $pageUid
     * @return bool
     */
    public function isAutoDumpEnabled(int $pageUid = null) : bool
    {
        try {
            $dumpIO = IO\DumpIOFactory::get($pageUid ?? $this->pageUid);
        } catch (IOException $e) {
            return false;
        }
        return $dumpIO instanceof IO\AbstractDumpIO &&
               $this->config->get('tx_deployable_records.autoDump', false, $pageUid ?? $this->pageUid);
    }

    /**
     * @param string $direction
     * @return bool
     * @throws DumpException
     * @throws IOException
     */
    public function syncAll(string $direction) : bool
    {
        $statusAll = true;
        $dumpStatusCollection = $this->getStatusForAll();
        /** @var DumpStatus $dumpStatus */
        foreach ($dumpStatusCollection->getRecordsOutOfSync() as $dumpStatus) {
            if ($dumpStatus->isNew()) {
                $status = $this->createDeployableRecord($dumpStatus->getIdentifier()); // TODO options
            } else {
                $status = $this->updateDeployableRecord(
                    $direction,
                    $dumpStatus->getTable(),
                    $dumpStatus->getIdentifier()
                );
            }
            if (!$status) {
                $statusAll = false;
            }
        }
        return $statusAll;
    }

    /**
     * Returns a DumpStatusCollection
     * filled with DumpStatus objects.
     *
     * @return DumpStatusCollection|DumpStatus[]
     * @throws DumpException
     * @throws IOException
     */
    public function getStatusForAll() : DumpStatusCollection
    {
        $allDeployableRecords = $this->deployableRecordRepository->findAll();
        $dumpStatus = [];
        foreach ($allDeployableRecords as $deployableRecord) {
            $dumpStatus[$deployableRecord->getIdentifier()] = $this->getDumpStatus($deployableRecord);
        }

        $dumpStatus += $this->getDumpStatusForNewEntries();

        return new DumpStatusCollection($dumpStatus);
    }

    /**
     * Convenience method to get DeployableRecord by table name and uid
     *
     * @param string $table
     * @param int $uid
     * @return DeployableRecord|null
     * @throws \Exception
     */
    public function getDeployableRecordByTableAndUid(string $table, int $uid)
    {
        return $this->deployableRecordRepository->findOneByTableAndUid($table, $uid);
    }

    /**
     * @return DumpStatus[]
     */
    private function getDumpStatusForNewEntries() : array
    {
        try {
            $dumpIO = IO\DumpIOFactory::get($this->pageUid);
        } catch (IOException $e) {
            return [];
        }
        $newEntries = DumpUtility::identifyNewEntriesInDump($dumpIO->getAll());
        $status = [];
        foreach ($newEntries as $identifier => $newEntry) {
            try {
                $status[] = new DumpStatus(
                    $newEntry['__table__'],
                    null,
                    [],
                    $newEntry,
                    $identifier
                );
            } catch (IOException $e) {
            }
        }
        return $status;
    }

    /**
     * You can get the status of a single deployable record,
     * by its identifier OR by its table name and uid.
     *
     * @param DeployableRecord|string|null $identifier or Deployable Record instance
     * @param string|null $table
     * @param int|null $uid
     * @return DumpStatus
     * @throws DumpException
     */
    public function getDumpStatus($identifier = null, string $table = null, int $uid = null) : DumpStatus
    {
        if (\is_object($identifier) && $identifier instanceof DeployableRecord) {
            $deployableRecord = $identifier;
            $table = $deployableRecord->getTable();
            $uid = $deployableRecord->getUid();
        } else {
            list($deployableRecord, $identifier, $table, $uid) = $this->resolveEmptyRecordAttributesAndDeployableRecord(
                $identifier,
                $table,
                $uid
            );
        }

        $row = $this->anyRecordRepository->findByTableAndUid($table, $uid);
        $dumpRow = [];
        if ($identifier) {
            try {
                $dumpRow = IO\DumpIOFactory::get($table === 'pages' ? $uid : $row['pid'] ?? 0)
                    ->getRow((string)$identifier);
            } catch (IOException $e) {
            }
        }
        return new DumpStatus($table, $deployableRecord, $row, $dumpRow, (string) $identifier);
    }

    /**
     * Adds new deployable record for existing database record
     *
     * @param string $identifier
     * @param string $table
     * @param int $uid
     * @param bool $fromDump
     * @param array $options
     * @return bool True on success. Otherwise throws exception.
     * @throws DumpException
     * @throws DumpReferenceException
     */
    public function addDeployableRecord(
        string $identifier,
        string $table,
        int $uid,
        bool $fromDump = false,
        array $options = []
    ) : bool {
        $row = $this->anyRecordRepository->findByTableAndUid($table, $uid);
        $deployableRecord = $this->deployableRecordRepository->findOneByTableAndUid($table, $uid);

        // Throws Exception if anything is not valid
        DumpUtility::checkIfTableIsAllowedAndRowIsValid($table, $row);
        DumpUtility::checkIfDeployableRecordAlreadyExists($deployableRecord);
        DumpUtility::checkIfIdentifierIsUnique($identifier);
        DumpUtility::checkIfParentPageIsStaged($row);
        if (!$fromDump) {
            $this->checkDumpFileForExistingIdentifier($identifier);
        }

        $newDeployableRecord = new DeployableRecord($identifier, $table, $uid, null);
        $statusDb = $this->deployableRecordRepository->add($newDeployableRecord);
        if (!$statusDb) {
            throw new DumpException('While writing to DB an error occurred.');
        }

        if ((!$fromDump && !empty($options)) || (!$fromDump && $this->isAutoDumpEnabled())) {
            $dumpIO = IO\DumpIOFactory::get($table === 'pages' ? $uid : $row['pid']);
            try {
                // Prepare row (filter attributes by PageTS config and resolve identifiers)
                $packedRow = DumpUtility::prepareDatabaseRow($row, $table);
                $dumpIO->writeRow($identifier, $packedRow, $options);
            } catch (IOException $exception) {
                // Revert created deployable record
                $this->deployableRecordRepository->remove($newDeployableRecord);
                throw new DumpException(
                    'Can\'t write row with identifier "' . $identifier .
                    '" to dump file "' . $dumpIO->getFilePath() . '" (Auto dump enabled)'
                );
            }
        }
        return true;
    }

    public function removeDeployableRecord()
    {
        // TODO Also update \T3\DeployableRecords\Ajax\RecordsAjaxController::removeAction
        // $status = $this->deployableRecordRepository->remove($deployableRecord);
        // Remove it from dump only?
    }

    /**
     * Creates a deployable record (and the database record itself) from dump
     *
     * @param string $identifier
     * @return bool
     * @throws DumpException
     * @throws IOException
     */
    public function createDeployableRecord(string $identifier) : bool
    {
        $newRecord = IO\DumpIOFactory::get($this->pageUid)->getRow($identifier);
        if (!$newRecord) {
            throw new DumpException('Can\'t find new record with identifier "' . $identifier . '" in dump file');
        }

        $newRecordUid = $this->anyRecordRepository->createRecordFromDumpRow($identifier, $newRecord);
        $options = []; // TODO
        return $this->addDeployableRecord($identifier, $newRecord['__table__'], $newRecordUid, true, $options);
    }

    /**
     * @param string $direction "db" or "dump" - where to update the record
     * @param string $table
     * @param string $identifier
     * @return bool
     * @throws DumpException
     * @throws IOException
     * @throws \Exception
     */
    public function updateDeployableRecord(
        string $direction,
        string $table,
        string $identifier,
        array $options = []
    ) : bool {
        if (!\in_array($direction, [self::FROM_DUMP_TO_DB, self::FROM_DB_TO_DUMP], true)) {
            throw new \UnexpectedValueException('Direction argument requires to be "db" or "dump".');
        }

        $deployableRecord = $this->deployableRecordRepository->findByIdentifier($identifier);
        if (!$deployableRecord) {
            throw new DumpException('No deployable record with identifier "' . $identifier . '" found!');
        }

        $dumpIO = IO\DumpIOFactory::get(
            $table === 'pages' ? $deployableRecord->getUid() : $deployableRecord->getDatabaseRow()['pid'] ?? 0
        );
        if ($direction === self::FROM_DB_TO_DUMP) {
            // TODO options
            return $dumpIO->writeRow(
                $identifier,
                DumpUtility::prepareDatabaseRow($deployableRecord->getDatabaseRow(), $table)
            );
        }
        return $this->anyRecordRepository->update(
            $table,
            $deployableRecord->getUid(),
            DumpUtility::prepareDumpRow($identifier, $dumpIO->getRow($identifier), $table)
        );
    }

    /**
     * @param string $identifier
     * @return void
     * @throws DumpException
     */
    private function checkDumpFileForExistingIdentifier(string $identifier)
    {
        if (!empty(IO\DumpIOFactory::get($this->pageUid)->getRow($identifier))) {
            throw new DumpException(
                'The dump file already contains a deployable record with identifier "' .
                $identifier . '".'
            );
        }
    }

    /**
     * @param string|null $identifier
     * @param string|null $table
     * @param int|null $uid
     * @return array
     * @throws DumpException
     */
    private function resolveEmptyRecordAttributesAndDeployableRecord(
        string $identifier = null,
        string $table = null,
        int $uid = null
    ) : array {
        if ($identifier) {
            $deployableRecord = $this->deployableRecordRepository->findByIdentifier($identifier);
            if (!$deployableRecord) {
                throw new DumpException('No deployable record with identifier "' . $identifier . '" found!');
            }
            if ($table !== $deployableRecord->getTable() || $uid !== $deployableRecord->getUid()) {
                throw new DumpException(
                    'Identifier "' . $identifier . '" does not match with given table "' . $table . '" and uid "' .
                    $uid . '"! The identifier points to ' . $deployableRecord->getTable() . ':' .
                    $deployableRecord->getUid() . '.'
                );
            }
            $table = $deployableRecord->getTable();
            $uid = $deployableRecord->getUid();
        } elseif ($table && $uid) {
            $deployableRecord = $this->deployableRecordRepository->findOneByTableAndUid($table, $uid);
            if ($deployableRecord) {
                $identifier = $deployableRecord->getIdentifier();
            }
        } else {
            throw new \UnexpectedValueException('Please provide $identifier or $table and $uid for this method!');
        }
        return [$deployableRecord, $identifier, $table, $uid];
    }
}
