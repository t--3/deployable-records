<?php declare(strict_types=1);
namespace T3\DeployableRecords\Dump;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */

class DumpException extends \Exception
{
}
