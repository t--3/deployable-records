<?php declare(strict_types=1);
namespace T3\DeployableRecords\Dump;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use Symfony\Component\OptionsResolver\OptionsResolver;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Defines which options a single dump entry can have.
 * Using Symfony's OptionsResolver.
 */
class DumpOptions
{
    public const OPTION_ADD_TO_TYPOSCRIPT = 'addToTypoScript';

    /**
     * @var array
     */
    protected $options;

    /**
     * @var array
     */
    protected $registeredOptions = [];

    /**
     * DumpOptions constructor
     *
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->registerOptions();

        /** @var OptionsResolver $resolver */
        $resolver = GeneralUtility::makeInstance(OptionsResolver::class);
        $this->configureOptions($resolver);
        $this->options = $resolver->resolve($options);
    }

    /**
     * Registers available options
     *
     * @return void
     */
    protected function registerOptions() : void
    {
        $this->registeredOptions = [
            static::OPTION_ADD_TO_TYPOSCRIPT => ['allowedTypes' => 'bool', 'default' => true]
        ];
    }

    /**
     * Configure options in OptionsResolver, using the registered options
     *
     * @param OptionsResolver $resolver
     * @return void
     */
    protected function configureOptions(OptionsResolver $resolver) : void
    {
        foreach ($this->registeredOptions as $name => $settings) {
            $resolver->setDefault($name, $settings['default'] ?? null);
            if (!empty($settings['required'])) {
                $resolver->setRequired($name);
            }
            if (!empty($settings['allowedTypes'])) {
                $resolver->setAllowedTypes($name, $settings['allowedTypes']);
            }
            if (!empty($settings['allowedValues'])) {
                $resolver->setAllowedValues($name, $settings['allowedValues']);
            }
        }
    }

    /**
     * Returns single option value
     *
     * @param string $key
     * @param mixed $default
     * @return mixed|null
     */
    public function get(string $key, $default = null)
    {
        return $this->options[$key] ?? $default;
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return $this->options;
    }
}
