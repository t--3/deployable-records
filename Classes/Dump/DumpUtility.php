<?php declare(strict_types=1);
namespace T3\DeployableRecords\Dump;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Configuration;
use T3\DeployableRecords\Domain\Model\DeployableRecord;
use T3\DeployableRecords\Domain\Repository\AnyRecordRepository;
use T3\DeployableRecords\Domain\Repository\DeployableRecordRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Static class RecordUtility
 */
class DumpUtility
{
    /**
     * Returns value of given path in pageTS of current page.
     *
     * @param string $table separated with dots. e.g.: "tx_deployable_records.output"
     * @param int $pageUid Optional. Set uid of page from which PageTS should get loaded.
     * @return bool True if the table is allowed by configuration, respecting given pageUid (pid)
     */
    public static function tableIsAllowed(string $table, int $pageUid = 0) : bool
    {
        $pageTsUtility = GeneralUtility::makeInstance(Configuration::class);
        /** @var array $tablesConfiguration Key is table name */
        $tablesConfiguration = $pageTsUtility->get('tx_deployable_records.tables', [], $pageUid);
        return array_key_exists($table, $tablesConfiguration);
    }

    /**
     * @param array $row
     * @param bool $throwException
     * @return bool
     * @throws DumpException #
     */
    public static function checkIfParentPageIsStaged(array $row, bool $throwException = true) : bool
    {
        if ((int) $row['pid'] === 0) {
            return true;
        }
        $deployableRecordRepository = GeneralUtility::makeInstance(DeployableRecordRepository::class);
        $deployableRecord = $deployableRecordRepository->findOneByTableAndUid('pages', (int) $row['pid']);
        if ($throwException && !$deployableRecord) {
            throw new DumpException(
                'The page which contains this record (pid: ' . $row['pid'] .
                '), is no deployable record. Please add the parent page first!'
            );
        }
        return $deployableRecord instanceof DeployableRecord;
    }

    /**
     * @param string $table
     * @param array $row
     * @return void
     * @throws DumpException
     */
    public static function checkIfTableIsAllowedAndRowIsValid(string $table, array $row)
    {
        if (!$row) {
            throw new DumpException('No valid record found.');
        }
        if (!static::tableIsAllowed($table, (int) $row['pid'])) {
            throw new DumpException('Table "' . $table . '" is not allowed by configuration.');
        }
    }

    /**
     * @param DeployableRecord|null $deployableRecord
     * @return void
     * @throws DumpException
     */
    public static function checkIfDeployableRecordAlreadyExists(DeployableRecord $deployableRecord = null)
    {
        if ($deployableRecord) {
            throw new DumpException(
                'A deployable record for table "' . $deployableRecord->getTable() . '" and uid "' .
                $deployableRecord->getUid() . '" with identifier "' . $deployableRecord->getIdentifier() .
                '" is already existing!'
            );
        }
    }

    /**
     * @param string $identifier
     * @return void
     * @throws DumpException
     */
    public static function checkIfIdentifierIsUnique(string $identifier)
    {
        $deployableRecordRepository = GeneralUtility::makeInstance(DeployableRecordRepository::class);
        $deployableRecord = $deployableRecordRepository->findByIdentifier($identifier);
        if ($deployableRecord) {
            throw new DumpException(
                'A deployable record with identifier "' . $identifier . '" already exists for "' .
                $deployableRecord->getTable() . ':' . $deployableRecord->getUid() . '".'
            );
        }
    }

    /**
     * Converts a database row (with numeric uids) to dump row, which has all uids replaced by identifiers
     * and the info from which __table__ the row is from
     *
     * @param array $databaseRow Database row with uids (int)
     * @param string $table
     * @param bool $resolveReferences
     * @return array
     * @throws DumpException
     * @throws DumpReferenceException
     */
    public static function prepareDatabaseRow(array $databaseRow, string $table, bool $resolveReferences = true) : array
    {
        $deployableRecordRepository = GeneralUtility::makeInstance(DeployableRecordRepository::class);
        $packedRow = array_merge(
            ['__table__' => $table],
            $resolveReferences
                ? $deployableRecordRepository->resolveReferringFields(
                    static::filterRowByAllowedFields($databaseRow, $table),
                    $table,
                    $databaseRow
                )
                : static::filterRowByAllowedFields($databaseRow, $table)
        );

        // Strip default values from packed row
        /** @var AnyRecordRepository $anyRecordRepository */
        $anyRecordRepository = GeneralUtility::makeInstance(AnyRecordRepository::class);
        $packedRow = array_diff($packedRow, $anyRecordRepository->getDefaultValuesForTable($table));

        unset($packedRow['uid']);
        return $packedRow;
    }

    /**
     * Converts dump row to database row
     *
     * @param string $identifier
     * @param array $dumpRow
     * @param string $table
     * @param bool $resolveReferences
     * @return array
     * @throws DumpException
     */
    public static function prepareDumpRow(
        string $identifier,
        array $dumpRow,
        string $table,
        bool $resolveReferences = true
    ) : array {
        $deployableRecordRepository = GeneralUtility::makeInstance(DeployableRecordRepository::class);
        $unpackedRow = $resolveReferences
            ? $deployableRecordRepository->unresolveReferringFields(
                $identifier,
                static::filterRowByAllowedFields($dumpRow, $table),
                $table
            )
            : static::filterRowByAllowedFields($dumpRow, $table);
        unset($unpackedRow['__table__']);
        return $unpackedRow;
    }

    /**
     * @param array $dumpContent
     * @return array
     */
    public static function identifyNewEntriesInDump(array $dumpContent) : array
    {
        $deployableRecordRepository = GeneralUtility::makeInstance(DeployableRecordRepository::class);
        $newEntriesInDump = [];
        if (array_key_exists('rows', $dumpContent)) {
            foreach ($dumpContent['rows'] as $identifier => $dumpedRow) {
                if (!$deployableRecordRepository->findByIdentifier($identifier)) {
                    $newEntriesInDump[$identifier] = $dumpedRow;
                }
            }
        }
        return $newEntriesInDump;
    }

    /**
     * Sanitizes identifier for Deployable Records
     *
     * @param string $identifier
     * @return string
     */
    public static function sanitizeIdentifier(string $identifier) : string
    {
        $identifier = str_replace(' ', '_', preg_replace('/[^a-z 0-9_-]/i', '', $identifier));
        return strtolower(htmlspecialchars($identifier));
    }

    /**
     * @param array $row
     * @param string $table
     * @return array
     */
    private static function filterRowByAllowedFields(array $row, string $table) : array
    {
        if (empty($row)) {
            return $row;
        }
        $allowedFields = static::getAllowedFields($table, (int) $row['pid']);
        $allowedFields['uid'] = '1';
        $allowedFields['pid'] = '1';
        return array_intersect_key($row, $allowedFields);
    }

    /**
     * @param string $table
     * @param int $pageUid
     * @return array
     */
    private static function getAllowedFields(string $table, int $pageUid = 0) : array
    {
        $allowedFields = [];
        if (static::tableIsAllowed($table, $pageUid)) {
            $config = GeneralUtility::makeInstance(Configuration::class);
            $allowedFields = $config->get('tx_deployable_records.tables.' . $table, [], $pageUid);

            $ctrl = $GLOBALS['TCA'][$table]['ctrl'];
            foreach ([
                'delete',
                'descriptionColumn',
                'editlock',
                'label',
                'languageField',
                'sortby',
                'transOrigPointerField',
                'type',
                'typeicon_column',
             ] as $value) {
                if (null !== $field = $ctrl[$value]) {
                    $allowedFields[$field] = '1';
                }
            }
            // Enablecolumns
            foreach ([
                'disabled',
                'endtime',
                'fe_group',
                'starttime'
            ] as $value) {
                if (null !== $field = $ctrl['enablecolumns'][$value]) {
                    $allowedFields[$field] = '1';
                }
            }
        }
        return $allowedFields;
    }
}
