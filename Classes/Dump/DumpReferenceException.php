<?php declare(strict_types=1);
namespace T3\DeployableRecords\Dump;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */

class DumpReferenceException extends DumpException
{
    /**
     * @var array
     */
    private $missingRelations;

    /**
     * DumpReferenceException constructor
     *
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     * @param array $missingRelations
     * @param string $fieldNameWithMissingRelations
     */
    public function __construct(
        string $message = '',
        int $code = 0,
        \Throwable $previous = null,
        array $missingRelations = []
    ) {
        parent::__construct($message, $code, $previous);
        $this->missingRelations = $missingRelations;
    }

    /**
     * @return array
     */
    public function getMissingRelations() : array
    {
        return $this->missingRelations;
    }

    /**
     * @return array
     */
    public function getMissingRelationsGrouped() : array
    {
        $missingRecords = [];
        foreach ($this->getMissingRelations() as $field => $items) {
            foreach ($items as $missingRelation) {
                $rowIdentifier = $missingRelation['table'] . ':' . $missingRelation['uid'];
                $missingRelation['key'] = isset($missingRecords[$rowIdentifier])
                    ? $missingRecords[$rowIdentifier] . ', ' . $field
                    : $field;
                $missingRecords[$rowIdentifier] = $missingRelation;
            }
        }
        return $missingRecords;
    }
}
