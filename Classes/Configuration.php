<?php declare(strict_types=1);
namespace T3\DeployableRecords;

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Dump\IO\IOException;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Extension configuration based on PageTS
 *
 * The default PageTS config is stored in EXT:deployable_records/Configuration/TSconfig/Default.typoscript
 * which is included globally when the extension is installed.
 */
class Configuration implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * @var array
     */
    private $pageTsContent = [];

    /**
     * @var TypoScriptService
     */
    private $typoScriptService;

    /**
     * PageTsUtility constructor
     */
    public function __construct()
    {
        $this->typoScriptService = GeneralUtility::makeInstance(TypoScriptService::class);
    }

    /**
     * Returns value of given path in pageTS of current page.
     *
     * @param string $path separated with dots. e.g.: "tx_deployable_records.output"
     * @param mixed $default Optional. Value which should be returned if path is not existing or value empty
     * @param int $pageUid Optional. Set uid of page from which PageTS should get loaded.
     * @return mixed
     */
    public function get(string $path, $default = null, int $pageUid = 0)
    {
        $pageUid = $pageUid ?: GeneralUtility::_GP('id');
        if (!isset($this->pageTsContent[$pageUid])) {
            $this->pageTsContent[$pageUid] = $this->typoScriptService->convertTypoScriptArrayToPlainArray(
                BackendUtility::getPagesTSconfig($pageUid)
            );
        }
        try {
            $value = ArrayUtility::getValueByPath($this->pageTsContent[$pageUid], $path, '.');
            if (is_array($value) && array_key_exists('_typoScriptNodeValue', $value)) {
                $value = $value['_typoScriptNodeValue'];
            }
        } catch (\Exception $e) {
            return $default;
        }
        return empty($value) ? $default : $value;
    }

    /**
     * Throws IOException if configuration is not properly!
     *
     * @return void
     * @throws IOException
     */
    public function isConfiguredCorrectly()
    {
        $configuredPath = $this->get('tx_deployable_records.output');
        if (!$configuredPath) {
            throw new IOException('No output path configured!');
        }
        $realPath = GeneralUtility::getFileAbsFileName($configuredPath);
        if (!$realPath) {
            throw new IOException('Can\'t resolve given path "' . $configuredPath . '"!');
        }
        if (file_exists($realPath) && !is_writable($realPath)) {
            throw new IOException('Configured dump path "' . $realPath . '" is not writable!');
        }
        if (!file_exists($realPath) && is_dir(\dirname($realPath)) && !is_writable(\dirname($realPath))) {
            throw new IOException('Configured dump directory "' . \dirname($realPath) . '" is not writable!');
        }
    }
}
