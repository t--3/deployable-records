<?php

return [
    'dr:sync' => [
        'class' => \T3\DeployableRecords\Command\SyncCommand::class,
        'schedulable' => true,
    ],
    'dr:status' => [
        'class' => \T3\DeployableRecords\Command\StatusCommand::class,
        'schedulable' => false
    ]
];
