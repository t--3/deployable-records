<?php

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
use T3\DeployableRecords\Ajax\RecordsAjaxController;

/*
 * Definitions for routes provided by EXT:deployable_records
 * Contains all AJAX-based routes for entry points
 */

return [

    'deployable_records_status' => [
        'path' => '/deployable_records/status',
        'target' => RecordsAjaxController::class . '::statusAction'
    ],
    'deployable_records_add' => [
        'path' => '/deployable_records/add',
        'target' => RecordsAjaxController::class . '::addAction'
    ],
    'deployable_records_update' => [
        'path' => '/deployable_records/update',
        'target' => RecordsAjaxController::class . '::updateAction'
    ],
    'deployable_records_upload' => [
        'path' => '/deployable_records/upload',
        'target' => RecordsAjaxController::class . '::uploadAction'
    ],
    'deployable_records_remove' => [
        'path' => '/deployable_records/remove',
        'target' => RecordsAjaxController::class . '::removeAction'
    ],
    'deployable_records_sync' => [
        'path' => '/deployable_records/sync',
        'target' => RecordsAjaxController::class . '::syncAction'
    ],
];
