# Deployable Records for TYPO3 CMS

This extension allows you to mark **important records** in your TYPO3 database as "deployable record". 
Each deployable record gets a unique **identifier** and is stored in a local file (called "dump").

Now, you can spread the dump over several environments or share it with colleagues (e.g. Git). Deployable Records
keeps the database and the dump in sync for you. Also, because we've defined a unique identifier for each record, 
we are able to work **without hardcoded uids**.


## Basics

### Requirements

- PHP 7.1 or higher
- TYPO3 8.7 LTS or 9.5 LTS


### Installation *(not released yet!)*

**Till release, just checkout master branch of this git repository.**

You can fetch EXT:deployable_records by adding "t3/deployable-records" as a dependency to your root composer.json.

    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/t--3/deployable-records.git"
        }
    ],
    "require": {
        "t3/cms": "^9.5",
        "t3/deployable-records": "dev-master"
    },
    
or on CLI:

    $ composer config repositories.deployable_records vcs https://bitbucket.org/t--3/deployable-records.git
    $ composer require t3/cms:"^9.5" t3/deployable-records:"dev-master"
           

You can also fetch and install it from TER (once released). 

When the extension is installed the default PageTS configuration is applied.


## Concept

### The Problem

When you are working in a professional environment you are probably using Git to version your code. Most likely you 
also use Git to deploy your code (direct or indirect, however...) to several environments/servers.

You try to outsource as much as possible to files, but there are some records left in the TYPO3 database, which are
absolutely necessary to get a proper output. For example:

- you need at least one page (`pages`)
- also, a typoscript configuration is needed (`sys_template`)
- and when you provide e.g. extended news functionality, you need:
  - Several pages (list, detail, archive, archive detail, RSS, ...) (`pages`)
  - and each page at least one news plugin instance (`tt_content`)

**So the main problem is:** How to get such important records from one instance to another?


### Deployable Records

The idea is to mark all **for the functionality of TYPO3 required records** as "deployable record". Each of these 
records gets a unique identifier (string). They are stored:

- in **tx_deployable_records** table in TYPO3 database of each instance (it stores the mapping between table name, uid 
  and identifier)
- In a local "dump" file (e.g. `fileadmin/dump.json` or better `EXT:project/Resources/Private/Data/Dump.json`)

The dump file should be added to Git and with a CLI task provided by this extension, you can sync the contents from dump 
to database, on any environment you want. This allows you to **integrate the content sync in your automated
deployment process**. Of course, you can also trigger the sync from within the backend. 


#### Identifier are the better uids

The concept of uids is a big problem when you need to sync content. This is the reason why you configure uids 
(record ids) in TypoScript configuration/constants on each environment separately. 

**With deployable records, you don't need to do this anymore!**

Inside of the dump file, all uids, pids and any other references to a record have been replaced by the corresponding 
identifier. **This requires that related and parent records (pages) also get staged as a deployable record!**

So instead of using hardcoded uids in your TypoScript, you can use the **automatically generated constants**.
In the following example a page, with identifier "newsDetail", has been added as deployable record:

```
plugin.tx_news.settings {
    limit = 30
    detailPid = {$pid.newsDetail}
    overrideFlexformSettingsIfEmpty := addToList(detailPid)
    startingpoint = {$pid.newsStorage}
}
```

## TYPO3 Integration

Deployable Records is well integrated to TYPO3 backend and CLI.


### Backend Module

For administrators and developers, there is a backend module, which gives you full control about deployable records.

- All deployable records on one view
- With detailed information about the sync status available
- Trigger sync of differences manually (from db to dump - or - from the dump to db)


### List View Integration

When you've added a table to PageTS configuration, a button will appear which allows you to mark this record
as deployable one. 

A popup will appear, which allows you to specify the identifier and further options.

Deployable record rows are visually highlighted in list view.


### CLI tools

To be able to trigger the sync process automatically, this extension provides a bunch of Extbase commands you can
call from CLI or add a task to TYPO3's Scheduler module.

#### Status 

Shows sync status of all staged deployable records.

```
vendor/bin/typo3 dr:status
```

#### Sync

Performs full sync in given direction ("db" or "dump").

```
vendor/bin/typo3 dr:sync --dump
```
or
```
vendor/bin/typo3 dr:sync --dump
```


### After Save Hook / Auto Dump

When a record, which has been marked as deployable changes, the dump file gets automatically updated, if the 
**auto dump** option is enabled. A flash message is displayed after each update.


### Status Panel

The global status of deployable records in the current TYPO3 CMS instance can get quickly checked with the 
**status panel** in the top area of the TYPO3 backend.


### AutoMagic TypoScript Constants

With the given unique identifier each deployable record has, you get an automatically added global constant containing
the uid of the related record. The given identifier is prefixed with `pid.` (configurable with PageTS). 

Example: `pid.newsDetail = 12`

Because on each environment new pages and other records may have a **different auto increment pointer**, you gain a real
benefit with this constant map.


## Future Features

### Local Identifier

You can also add one or more **local identifiers** to the deployable record. The regular identifier must be unique, the
local identifier not.

So let's say you have got two **site roots** in your TYPO3 installation. The main website and a landing page, with
own domain record. Both should display the same news.

Now you can add a news detail page in both site roots and give each of them a **unique identifier** and the same local
identifier. Example:

- newsDetail (*Identifier*)
    - newsDetail (*Local Identifier, optional because == identifier*)
- newsDetailLandingPage (*Identifier*)
    - newsDetail  (*Local Identifier*)

With this local identifier you get another auto TypoScript constant:

`pid.local.newsDetail = 12`

and

`pid.local.newsDetail = 36`

Each of those lines gets applied to TypoScript template for each **site root** (flag in page settings) defined.


### TYPO3_CONTEXT support

When marking a record as deployable, you can choose on which TYPO3 context it should apply. This allows you to create 
some example records (e.g. news records), limit them to *Development* and *Testing* context and share them with other 
developers. But you can be sure that they won't get applied to the database, when in *Production* context.


### Logging

Currently, you don't know why the dump has been updated. With the logging feature, you can see who or what triggered
a sync, and which records and data get affected. 

Maybe this feature can get extended, to create migration files, but I don't see a big benefit here unless you want to 
decouple the dumps from VCS (which I really would not recommend!)


### Signals & Slots

With SignalSlotDispatcher of TYPO3, you can make your code extendable. Deployable Records will provide various slots,
to be able to modify attributes before saving, trigger actions, notifications, etc.
