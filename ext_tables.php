<?php

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */

defined('TYPO3_MODE') or die();

$boot = function (string $extKey, array $extConf) {
    // Register backend module
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'T3.' . $extKey,
        'tools',
        'module',
        '',
        [
            'RecordsModule' => 'index,records,configuration'
        ],
        [
            'access' => 'user,group',
            'icon' => 'EXT:' . $extKey . '/ext_icon.svg',
            'labels' => 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang.xlf',
        ]
    );

    // Register extension icon
    /** @var \TYPO3\CMS\Core\Imaging\IconRegistry $iconRegistry */
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'ext-deployable-records-icon',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:' . $extKey . '/ext_icon.svg']
    );
    $iconRegistry->registerIcon(
        'ext-deployable-records-icon-white',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:' . $extKey . '/Resources/Public/Icons/ext_icon_white.svg']
    );

    // Register Global Stylesheets for Backend
    if (TYPO3_MODE === 'BE') {
        $GLOBALS['TBE_STYLES']['skins']['backend']['stylesheetDirectories'][$extKey] =
            'EXT:' . $extKey . '/Resources/Public/Styles/Global';
    }
};
$boot($_EXTKEY, !empty($_EXTCONF) ? unserialize($_EXTCONF, ['allowed_classes' => false]) : []);
unset($boot);
