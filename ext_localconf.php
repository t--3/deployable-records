<?php

/*  | This extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2018-2019 Armin Vieweg <armin@v.ieweg.de>
 */
defined('TYPO3_MODE') or die();

use T3\DeployableRecords\Hooks\Buttons\ListRowButtonHook;
use T3\DeployableRecords\Hooks\ProcessDatamap;
use T3\DeployableRecords\Hooks\TemplateService;
use T3\DeployableRecords\XClasses\DatabaseRecordList as DatabaseRecordListXClass;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Recordlist\RecordList\DatabaseRecordList;

$boot = function (string $extKey, array $extConf) {

    // Check for required vendor packages
    $autoloader = ExtensionManagementUtility::extPath($extKey, 'Resources/Private/Php/vendor/autoload.php');
    if (!class_exists('\Usox\Circulon\Circulon') && file_exists($autoloader)) {
        require_once $autoloader;
    }

    // Register default PageTS
    ExtensionManagementUtility::addPageTSConfig(
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $extKey . '/Configuration/TSconfig/Default.typoscript">'
    );

    // Add additional PageTS and classes/backports for TYPO3 8.7 compatibility
    if (VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) <
        VersionNumberUtility::convertVersionNumberToInteger('9.0.0')
    ) {
        ExtensionManagementUtility::addPageTSConfig(
            '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $extKey . '/Configuration/TSconfig/Default-8.7.typoscript">'
        );

        $compatibilityPath = ExtensionManagementUtility::extPath($extKey, 'Classes/Compatibility/');
        require_once $compatibilityPath . 'UriViewHelper.php';
        require_once $compatibilityPath . 'PageTreeRepository.php';
        require_once $compatibilityPath . 'EditRecordViewHelper.php';
    }

    // Allow to override default PageTS by extension settings
    if (isset($extConf['outputFile'])) {
        $path = $extConf['outputFile'];
        if ($path) {
            ExtensionManagementUtility::addPageTSConfig(
                'tx_deployable_records.output = ' . $extConf['outputFile']
            );
        }
    }
    if (isset($extConf['dumpIO'])) {
        $path = $extConf['dumpIO'];
        if ($path) {
            ExtensionManagementUtility::addPageTSConfig(
                'tx_deployable_records.dumpIO = ' . $extConf['dumpIO']
            );
        }
    }

    ExtensionManagementUtility::addPageTSConfig(
        'tx_deployable_records.autoDump = ' .
        (isset($extConf['autoDump']) && (bool) $extConf['autoDump'] === true ? '1' : '0')
    );

    // Include additional PageTS configuration file, if configured in extension settings
    if (isset($extConf['additionalPageTS'])) {
        $path = GeneralUtility::getFileAbsFileName(
            $extConf['additionalPageTS']
        );
        if ($path && file_exists($path)) {
            ExtensionManagementUtility::addPageTSConfig(
                '<INCLUDE_TYPOSCRIPT: source="FILE:' . $extConf['additionalPageTS'] . '">'
            );
        }
    }

    if (TYPO3_REQUESTTYPE & TYPO3_REQUESTTYPE_BE) {
        GeneralUtility::makeInstance(PageRenderer::class)->addRequireJsConfiguration([
            'shim' => [
                'handlebars' => ['exports' => 'Handlebars']
            ],
            'paths' => [
                'handlebars' => PathUtility::getAbsoluteWebPath(
                    ExtensionManagementUtility::extPath($extKey, 'Resources/Public/JavaScript/Contrib/')
                ) . 'Handlebars'
            ]
        ]);
    }

    // Register XClass
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][DatabaseRecordList::class] = [
        'className' => DatabaseRecordListXClass::class
    ];

    // Register Hooks
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['typo3/class.db_list_extra.inc']['actions'][$extKey] =
        ListRowButtonHook::class;

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['Backend\Template\Components\ButtonBar']['getButtonsHook'][$extKey] =
        \T3\DeployableRecords\Hooks\Buttons\ButtonBarHook::class . '->add';

    // Generate TypoScript (based on mapped identifiers/uids)
    if (isset($extConf['generateTypoScript']) && $extConf['generateTypoScript']) {
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']
        ['Core/TypoScript/TemplateService']['runThroughTemplatesPostProcessing'][] =
            TemplateService::class . '->runThroughTemplatesPostProcessing';
    }

    // After save hook (for autoDump)
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][$extKey] =
        ProcessDatamap::class;

    // Register Status Panel toolbar item
    if (isset($extConf['enableBackendStatusPanel']) && $extConf['enableBackendStatusPanel']) {
        $GLOBALS['TYPO3_CONF_VARS']['BE']['toolbarItems'][$extKey] = \T3\DeployableRecords\Backend\ToolbarItem::class;
    }
};
$boot($_EXTKEY, !empty($_EXTCONF) ? unserialize($_EXTCONF, ['allowed_classes' => false]) : []);
unset($boot);
